# 编写环境

1. **编辑器VSCode：** https://code.visualstudio.com/
1. **文档语言Markdown：**
    1. **Markdown-Preview-Enhanced官网：** https://shd101wyy.github.io/markdown-preview-enhanced/#/
    1. **VSCode Markdown-Preview-Enhanced插件：** https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced
1. **图示语言：**
    1. **PlantUML：**
        1. **PlantUML官网：** https://plantuml.com/
        1. **VSCode PlantUML插件：** https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
    1. **Graphviz：**
        1. **Graphviz官网：** https://www.graphviz.org/
        1. **VSCode Graphviz插件：** https://marketplace.visualstudio.com/items?itemName=EFanZh.graphviz-preview

:warning: :warning: :warning: 在VSCode中使用PlantUML和Graphviz需先安装Graphviz应用程序（[各平台的Graphviz下载地址](http://graphviz.org/download/)）并设置Path:warning: :warning: :warning: 
