# 使用方法

## 方式一：使用markdown文件

因为使用了[Markdown Preview Enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/)（以下简称“MPE”）的一些扩展语法，因此建议使用该插件进行预览。

## 方式二：使用已导出的html文件

1. 使用`Chrome`或`Firefox`打开（**`IE`无法正常打开**），在`Windows`平台下，大多数情况`Firefox`阅读效果更佳（`Firefox`下`emoji`能完整显示，`Chrome`在有些机器上不能完整显示，具体原因未细究）
1. 可能需联网（课件基于的`revealjs`可能需要联网环境，导出为`cdn hosted`模式）

:warning:由于自定义了MPE的CSS，因此，您本地使用MPE预览的结果可能与已导出的html文件的显示不一样。
