# Difference Between C/C++ and Java

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Overview](#overview)
2. [C vs C++](#c-vs-c)
3. [C/C++ vs Java](#cc-vs-java)
    1. [main entry point](#main-entry-point)
    2. [Data Type](#data-type)
        1. [String](#string)
        2. [Float](#float)
        3. [Compound Data Type](#compound-data-type)
        4. [Type Casting](#type-casting)
    3. [Keyword](#keyword)
    4. [Operator](#operator)
    5. [Code Structure](#code-structure)
    6. [Generic Programming](#generic-programming)
    7. [Assertion](#assertion)
4. [C++ vs Java](#c-vs-java)
5. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## Overview

## C vs C++

## C/C++ vs Java

### main entry point

```c{.line-numbers}
int main(int argc, char * argv[]){
    return 0;
}
```

```java{.line-numbers}
public static void main(String args[]){
}
```

1. 形参数量： C/C++两个形参，Java一个形参
1. CLI参数传递： C/C++的CLI参数从argv[1]开始存储，Java的CLI参数从args[0]开始存储

### Data Type

#### String

C/C++ support ASCII strings, wide chars. Java supports Unicode chars.

#### Float

#### Compound Data Type

1. C/C++ support pointers i.e. *, -> and dot (.). Java only uses references i.e. dot (.)

#### Type Casting

1. Java is strong-typed language. Implicit type casting doesn't happen.

### Keyword

Most of the keywords are common. C++ and Java have their own additional key words.

### Operator

1. Java supports one extra bit-wise operator >>>

### Code Structure

1. C/C++ include header files, whereas Java imports classes.
1. Java is object-oriented, C++ is object-based (due to its backward compatibility for C), C is procedural. Hence you need to use object-oriented syntax in C++/Java.
1. No pre-processor in Java.

### Generic Programming

1. C++ has templates & containers, it happen in compile time and it is a real generic programming.
1. Java has generics & collections, it is based on Object class, and it is not a real generic programming.

### Assertion

1. assert in C/C++ is a library function, while assert in Java is keyword
1. assert can be disable in compile time in C/C++, while only be disable in runtime in Java

## C++ vs Java

## Bibliographies

1. Java与C++的异同点总结. https://blog.csdn.net/SHENNONGZHAIZHU/article/details/51897060
1. C++与Java的语法区别. https://blog.csdn.net/xylary/article/details/1678413
1. java和C++的区别总结（一）. https://blog.csdn.net/qq_14982047/article/details/50769133
1. C++与Java的区别. https://www.jianshu.com/p/ce971b8a2e52
