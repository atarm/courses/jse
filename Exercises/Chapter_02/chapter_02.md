# Chapter02 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读或调试程序](#阅读或调试程序)
4. [编写程序](#编写程序)

<!-- /code_chunk_output -->

## 问答题

1. 什么叫标识符？标识符的规则是什么？false是否可以作为标识符？ => 简而言之，标识符identifer是用于标识一个程序元素（类、接口、变量、方法等）的一个字符序列（非关键字、非保留字、预定义字面量），标识符由字母字符（非英文也可，但不建议）、下画线、美元符号和数字（首字符不能为数字）组成，`false`、`true`、`null`是预定义字面量，不能用作标识符
2. 什么叫关键字？true和false是否是关键字？请说出6个关键字？ => 关键字指在编程语言中有特定意义的字符序列
>see more: https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
3. Java的基本数据类型都是什么？=> 布尔：`boolean`；字符：`char`；整型（均为`signed`）：`byte`, `short`, `int`, `long`；浮点： `float`, `double`
4. float型常量和double型常量在表示上有什么区别？ => `float`字面量以`F`或`f`做后缀，`double`字面量以`D`或`d`做后缀，**`float`字面量后缀不可省略**，`double`字面量后缀可省略
5. 怎样获取一维数组的长度,怎样获取二维数组中一维数组的个数。 => 一维数组名.length，二维数组名.length

:warning::exclamation:**常量（Constant）** 与 **字面量（literal）** 并不是同一个概念，常量是只读变量（C/C++用`const`修饰，Java用`final`修饰），字面量是直接写在源代码的值

## 选择题

1. **下列哪项字符序列可以做为标识符？**
A. true
B. default
C. _int
D. good-class

答：`C`
解：
A => `true`是预定义字面量
B => `default`是关键字
D => `-`不能作为标识符中的字符

2. **下列哪三项是正确的float变量的声明？**
A. float foo = -1;
B. float foo = 1.0;
C. float foo = 42e1;
D. float foo = 2.02f;
E. float foo = 3.03d;
F. float foo = 0x0123;

答：`ADF`
解：
A => `-1`是`int`字面量，可自动转型（隐式类型转换）至`float`
B => `1.0`为`double`字面量（`float`字面量后缀不可省略）
C => `42e`为`double`字面量（`float`字面量后缀不可省略）
D => 略
E => `double`不能自动转型至`float`（`double`的值域比`float`大，需要强制转型）
F => `0x0123`是`int`字面量（16进制表示法）

3. **下列哪一项叙述是正确的？**
A. char型字符在Unicode表中的位置范围是0至32767
B. char型字符在Unicode表中的位置范围是0至65535
C. char型字符在Unicode表中的位置范围是0至65536
D. char型字符在Unicode表中的位置范围是-32768至32767

答：`B`
解：`char`代表Unicode编码（即在Unicode字符集中索引值，或称为偏移值）

4. **以下哪两项是正确的char型变量的声明？**
A. char ch = "R";
B. char ch = '\\';
C. char ch = 'ABCD';
D. char ch = "ABCD";
E. char ch = '\ucafe';
F. char ch = '\u10100';

答：`BE`
解：
A => 双引号包围的是`String`
B => 表示反斜杠（单个反斜杠是转义前导符）
C => `char`只能容纳单个字符
D => `"ABCD"`是`String`
E => Unicode十六进制表示法，详见:book:P20
F => Unicode十六进制表示法只能用4个十六进制字符，而`10100`是5个字符

5. **下列程序中哪些【代码】是错误的？**

```java{.line-numbers}
public class E {
   public static void main(String args[]) {
      int x = 8;
      byte b = 127;     //【代码1】
      b = x;           //【代码2】
      x = 12L;         //【代码3】
      long y=8.0;       //【代码4】  
      float z=6.89 ;     //【代码5】
   }
}
```

答：`【代码2】、【代码3】、【代码4】、【代码5】`
解：
【代码2】 => `int`到`byte`不能自动转型
【代码3】 => `long`到`int`不能自动转型
【代码4】 => `double`到`long`不能自动转型
【代码5】 => `double`到`float`不能自动转型

6. **对于int a[] = new int[3];下列哪个叙述是错误的？**
A. a.length的值是3。
B. a[1]的值是1。
C. a[0]的值是0。
D. a[a.length-1]的值等于a[2]的值。

答：`B`
解：
A => 略
B => `a[1]`的值是0（数组元素默认值，详见:book:P27）
C => 略
D => `a.length == 3`

## 阅读或调试程序

### 上机运行下列程序，注意观察输出的结果

```java{.line-numbers}
public class E {
 public static void main (String args[ ]) {
      for(int i=20302;i<=20322;i++) {
          System.out.println((char)i);
      }
    }
}
```

答：略

### 上机调试下列程序，注意System.out.print()和System.out.println()的区别

```java{.line-numbers}
public class OutputData { 
   public static void main(String args[]) {
      int x=234,y=432; 
      System.out.println(x+"<"+(2*x)); 
      System.out.print("我输出结果后不回车"); 
      System.out.println("我输出结果后自动回车到下一行");
      System.out.println("x+y= "+(x+y));
     }
}
```

答：略

### 上机调试下列程序，了解基本数据类型数据的取值范围

```java{.line-numbers}
public class E { 
   public static void main(String args[]) {
      System.out.println("byte取值范围:"+Byte.MIN_VALUE+"至"+Byte.MAX_VALUE); 
      System.out.println("short取值范围:"+Short.MIN_VALUE+"至"+Short.MAX_VALUE);  
      System.out.println("int取值范围:"+Integer.MIN_VALUE+"至"+Integer.MAX_VALUE); 
      System.out.println("long取值范围:"+Long.MIN_VALUE+"至"+Long.MAX_VALUE); 
      System.out.println("float取值范围:"+Float.MIN_VALUE+"至"+Float.MAX_VALUE); 
      System.out.println("double取值范围:"+Double.MIN_VALUE+"至"+Double.MAX_VALUE); 
  }
}
```

答：略

### 下列程序标注的【代码1】，【代码2】的输出结果是什么？

```java{.line-numbers}
public class E {
   public static void main (String args[ ]){
      long[] a = {1,2,3,4};
      long[] b = {100,200,300,400,500};
      b = a;
      System.out.println("数组b的长度:"+b.length); //【代码1】
      System.out.println("b[0]="+b[0]); //【代码2】
   }
}
```

答：
`【代码1】 => 数组b的长度:4`
`【代码2】 => b[0]=1`
解： 执行完`b = a;`后，引用变量`b`与`a`引用了同一个数组对象`{1,2,3,4}`

### 下列程序标注的【代码1】，【代码2】的输出结果是什么

```java{.line-numbers}
public class E {
   public static void main(String args[]) {
      int [] a={10,20,30,40}, b[]={{1,2},{4,5,6,7}};
      b[0] = a;
      b[0][1] = b[1][3];
      System.out.println(b[0][3]); //【代码1】
      System.out.println(a[1]);   //【代码2】
   }
}
```

答：
`【代码1】 => 40`
`【代码2】 => 7`
解：
执行完`b[0] = a;`后`b[] => {{10,20,30,40}, {4,5,6,7}}`
执行完`b[0][1] = b[1][3];`后`b[] => {{10,7,30,40}, {4,5,6,7}}`，由于`a`与`b[0]`引用同一个数组对象，故修改`b[0]`引用的对象与修改`a`引用的对象效果一致

## 编写程序

### 编写一个应用程序，给出汉字‘你’、‘我’、‘他’在Unicode表中的位置

答：略

### 编写一个Java应用程序，输出全部的希腊字母

答：略
