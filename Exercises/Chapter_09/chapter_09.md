# Chapter09 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [编程题](#编程题)

<!-- /code_chunk_output -->

## 问答题

1. `JFrame`类的对象的默认布局是什么布局？ => `BorderLayout`
2. 一个容器对象是否可以使用`add`方法添加一个`JFrame`窗口？ => 不可以
3. `JTextField`可以触发什么事件？ => `ActionEvent`, `FocusEvent`等等（详见`addXXXListener`）
4. `JTextArea`中的文档对象可以触发什么类型的事件？ => `DocumentEvent`等等（详见`addXXXListener`）
5. `MouseListener`接口中有几个方法？ => 5
6. 处理鼠标拖动触发的`MouseEvent`事件需使用哪个接口？ => `MouseMotionListener`

## 选择题

1. **下列哪个叙述是不正确的？**
A. 一个应用程序中最多只能有一个窗口
B. `JFrame`创建的窗口默认是不可见的
C. 不可以向`JFrame`窗口中添加`JFrame`窗口
D. 窗口可以调用`setTitle(String s)`方法设置窗口的标题

答：`A`
解：
A=>应用程序的窗口数量受操作系统限制
B=>调用`public void setVisible(boolean b)`使窗口可见
C=>语法上无错误，语义上有错误，运行时抛出运行时异常`IllegalArgumentException`
`Exception in thread "main" java.lang.IllegalArgumentException: adding a window to a container`

:warning::exclamation:教材选项C中的`JFame`应为`JFrame`
:warning::exclamation:**《实验指导与习题解答》的参考答案有误（C => A）**

2. **下列哪个叙述是不正确的？**
A．`JButton`对象可以使用使用`addActionLister(ActionListener l)`方法将没有实现`ActionListener`接口的类的实例注册为自己的监视器
B．对于有监视器的`JTextField`文本框，如果该文本框处于活动状态（有输入焦点）时，用户即使不输入文本，只要按回车（Enter）键也可以触发`ActionEvent`事件
C．监视`KeyEvent`事件的监视器必须实现`KeyListener`接口
D．监视`WindowEvent`事件的监视器必须实现`WindowListener`接口

答：`A`
解：
A=>语法错误
CD=>也可继承`xxxAdapter`间接实现`xxxListener`

3. **下列哪个叙述是不正确的？**
A．使用`FlowLayout`布局的容器最多可以添加5个组件
B．使用`BorderLayout`布局的容器被划分成5个区域
C．`JPanel`的默认布局是`FlowLayout`布局
D．`JDialog`的默认布局是`BorderLayout`布局

答：`A`
解：
A=>`FlowLayout`没有明确的数量限制（受限于运行机器的资源）

## 编程题

1. 编写应用程序，有一个标题为“计算”的窗口，窗口的布局为FlowLayout布局。窗口中添加两个文本区，当我们在一个文本区中输入若干个数时，另一个文本区同时对你输入的数进行求和运算并求出平均值，也就是说随着你输入的变化，另一个文本区不断地更新求和及平均值。

2. 编写一个应用程序，有一个标题为“计算”的窗口，窗口的布局为FlowLayout布局。设计四个按钮，分别命名为“加”、“差”、“积、”、“除”，另外，窗口中还有三个文本框。单击相应的按钮，将两个文本框的数字做运算，在第三个文本框中显示结果。要求处理NumberFormatException异常。

3. 参照例子15编写一个体现MVC结构的GUI程序。首先编写一个封装梯形类，然后再编写一个窗口。要求窗口使用三文本框和一个文本区为梯形对象中的数据提供视图，其中三个文本框用来显示和更新梯形对象的上底、下底和高；文本区对象用来显示梯形的面积。窗口中有一个按钮，用户单击该按钮后，程序用3个文本框中的数据分别作为梯形对象的上底、下底和高，并将计算出的梯形的面积显示在文本区中。
