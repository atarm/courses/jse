# Chapter08 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编程题](#编程题)

<!-- /code_chunk_output -->

## 问答题

1. `"\hello"`是正确的字符串常量吗？ => 不是（没有`'\h'`此种转义字符，正确应为`"\\hello"`）
1. `"你好KU".length()`和`"\n\t\t".length()`的值分别是多少？=>`4` , `3`
1. `"Hello".equals("hello")`和`"java".equals("java")`的值分别是多少？=>`false`,`true`
1. `"Bird".compareTo("Bird fly")`的值是正数还是负数？=>负数（`"Bird"<"Bird fly"`）
1. `"I love this game".contains("love")`的值是true吗？=>`true`
1. `"RedBird".indexOf("Bird")`的值是多少？`"RedBird".indexOf("Cat")`的值是多少？=>`3` , `-1`
1. 执行`Integer.parseInt("12.9");`会发生异常吗？=> 会抛出`NumberFormatException`

## 选择题

1. **下列哪个叙述是正确的？**
A. String 类是final 类，不可以有子类
B. String 类在java.util包中
C. "abc"=="abc"的值是false
D. "abc".equals("Abc")的值是true

答：`A`
解：
B => `java.lang`
C => `true`，指向`text segment`的同一内存地址
D => `false`

2. **下列哪个表达式是正确的（无编译错误）？**
A. int m = Float.parseFloat("567");
B. int m = Short.parseShort("567");
C. byte m = Integer.parseInt("2");
D. float m = Float.parseDouble("2.9");

答：`B`
解：
A => 引用变量类型应为`float`
C => 引用变量类型应为`int`
D => 应为`Float.parseFloat("2.9")`

:warning::exclamation:**《实验指导与习题解答》的参考答案有误（C ==应改为== B）**

3. **对于如下代码，下列哪个叙述是正确的？**
A. 程序编译出现错误
B. 程序标注的【代码】的输出结果是bird
C. 程序标注的【代码】的输出结果是fly
D. 程序标注的【代码】的输出结果是null

```java{.line-numbers}
public class E{
   public static void main(String[] args){ 
      String strOne = "bird";
      String strTwo = strOne;
      strOne = "fly";
      System.out.println(strTwo);
  }
}
```

答：`B`
解：第5行执行完后，`strTwo`仍指向字面量`"bird"`

4. **对于如下代码，下列哪个叙述是正确的？**
A. 程序出现编译错误
B. 无编译错误，在命令行执行程序：“java E I love this game”，程序输出this
C. 无编译错误，在命令行执行程序：“java E let us go”，程序无运行异常
D. 无编译错误，在命令行执行程序：“java E 0 1 2 3 4 5 6 7 8 9”程序输出3

```java{.line-numbers}
public class E {
    public static void main (String args[]) {
        String s1 = args[1];
        String s2 = args[2];
        String s3 = args[3];
        System.out.println(s3);
   }
}
```

答：`D`
解：
A => 无编译错误
B => 程序输出`game`
C => 程序出现`ArrayIndexOutOfBoundsException`异常

:warning:Java从args[0]开始存储参数，C/C++从argv[1]开始存储参数，argv[0]存储命令（可执行程序）名称本身

5. **下列哪个叙述是错误的？**
A. "9dog".matches("\\ddog")的值是true。
B."12hello567".replaceAll("[123456789]+","@")返回的字符串是@hello@。
C.new Date(1000)对象含有的时间是公元后1000小时的时间
D. "\\hello\n"是正确的字符串常量。

答：`C`
解：C => 含有的时间是`1000 milliseconds since January 1, 1970, 00:00:00 GMT`

## 阅读程序

### 请说出E类中标注的【代码】的输出结果。

```java{.line-numbers}
public class E {
   public static void main (String[]args)   {
      String str = new String ("苹果");
      modify(str);
      System.out.println(str);   //【代码】
   }
   public static void modify (String s)  {
      s = s + "好吃";
   }
}
```

答：`苹果`
解：
`String`是内容不可变对象，即每次修改`String`引用变量时，不是修改该引用变量引用的对象的内容，而是生成一个新的`String`对象，然后该`String`引用变量引用新的`String`对象

调用`public static void modify (String s)`时，实参`str`和形参`s`都指向同一个`String`对象，执行`s = s + "好吃";`后，形参`s`引用了一个新的`String`对象，而实参`str`引用的`String`对象内容没有发生变化

### 请说出E类中标注的【代码】的输出结果。

```java{.line-numbers}
import java.util.*;
class GetToken {
   String s[];
   public String getToken(int index,String str) {
      StringTokenizer fenxi = new StringTokenizer(str);
      int number = fenxi.countTokens();
      s = new String[number+1];
      int k = 1;
      while(fenxi.hasMoreTokens()) {
         String temp=fenxi.nextToken();
         s[k] = temp;
         k++;
      }
      if(index<=number)
         return s[index];
      else
         return null;
   }
}
class E {
  public static void main(String args[]) {
      String str="We Love This Game";
      GetToken token=new GetToken();
      String s1 = token.getToken(2,str), s2 = token.getToken(4,str);
      System.out.println(s1+":"+s2);     //【代码】
   }
}
```

答：`Love:Game`
解：
`public String getToken(int index,String str)`以默认分隔符切分成`String`数组（从下标1开始）

`s1`的值是第2个token（即`Love`），`s2`的值是第4个token（即`Game`）

### 请说出E类中标注的【代码1】，【代码2】的输出结果。

```java{.line-numbers}
public class E {
   public static void main(String args[]) {
      byte d[]="abc我们喜欢篮球".getBytes();
      System.out.println(d.length);   //【代码1】
      String s=new String(d,0,7);
      System.out.println(s);         //【代码2】
   }
}
```

答：
1. `gb2312/gbk`字符集下：
   + 【代码1】 => `15`
   + 【代码2】=> `abc我们`
1. `utf-8`字符集下：
   + 【代码1】 => `21`
   + 【代码2】=> `abc我�`（最后一个字符是乱码）

解：
```java{.line-numbers}
//java.lang.String
public byte[] getBytes()
/**Encodes this String into a sequence of bytes \
   using the platform's default charset, \
   storing the result into a new byte array.*/
```

`String`的`getBytes()`使用运行时平台的默认字符集编码成字节数组，`gb2312/gbk`字符集一个英文字符编码成一个字节，一个中文字符编码成两个字节，`utf-8`字符集一个英文字符编码成一个字节，一个中文字符编码成三个字节

【代码2】由于最后一个字节不能完整成解码成`utf-8`的字符，因此成为乱码

:warning::exclamation:**《实验指导与习题解答》的参考答案不全面**

### 请说出E类中标注的【代码】的输出结果。

```java{.line-numbers}
class MyString {
  public String getString(String s) {
      StringBuffer str = new StringBuffer();
      for(int i=0;i<s.length();i++) {
         if(i%2==0) {
            char c = s.charAt(i);
            str.append(c);
         }
      }
      return new String(str);
   }
}
public class E {
   public static void main(String args[ ]) {
      String s = "1234567890";
      MyString ms = new MyString();
      System.out.println(ms.getString(s)); //【代码】
   }
}
```

答：`13579`
解：
`public String getString(String s)`抽取形参`s`偶数下标的字符生成并返回一个新的`String`对象

### 请说出E类中标注的【代码】，的输出结果。

```java{.line-numbers}
public class E {
  public static void main (String args[ ]) {
      String regex = "\\djava\\w{1,}" ;
      String str1 = "88javaookk";
      String str2 = "9javaHello";
      if(str1.matches(regex)) {
          System.out.println(str1);
      }
      if(str2.matches(regex)) {
          System.out.println(str2); //【代码】
      }  
   }
}
```

答：`9javaHello`
解：
`"\\d"`代表数字，`"\\w{1,}"`代表不少于1个的字符（`[a-zA-Z_0-9]`）

:exclamation:正则表达式中预定义字符类（predefined character classes）以反斜杠`\`开头，反斜杠`\`在字符串中表示转义，因此，使用字符串表达正则表达式所需的`\`需要用两个反斜杠表示（即`"\\"`）

### 上机实习下列程序

学习怎样在一个月内（一周内、一年内）前后滚动日期，例如，假设是3月（有31天）10号，如果在月内滚动，那么向后滚动10天就是3月20日，向后滚动25天，就是3月4号（因为只在该月内滚动）。如果在年内滚动，那么向后滚动25天，就是4月4号。

```java{.line-numbers}
import java.util.*;
public class RollDayInMonth {
   public static void main(String args[]) {
      Calendar calendar=Calendar.getInstance();
      calendar.setTime(new Date());  
      String s = String.format("%tF(%<tA)",calendar);
      System.out.println(s);
      int n = 25;
      System.out.println("向后滚动(在月内)"+n+"天");
      calendar.roll(calendar.DAY_OF_MONTH,n);
      s = String.format("%tF(%<ta)",calendar);
      System.out.println(s);
      System.out.println("再向后滚动(在年内)"+n+"天");
      calendar.roll(calendar.DAY_OF_YEAR,n);
      s = String.format("%tF(%<ta)",calendar);
      System.out.println(s);
    }  
}
```

解：略

### 上机实习下列程序

（学习Runtime类），注意观察程序的输出结果。

```java{.line-numbers}
public class Test{
    public static void main(String args[]) {
        Runtime runtime = Runtime.getRuntime();
        long free = runtime.freeMemory();
        System.out.println("Java虚拟机可用空闲内存 "+free+" bytes");
        long total = runtime.totalMemory();
        System.out.println("Java虚拟机占用总内存 "+total+" bytes");
        long n1 = System.currentTimeMillis();
        for(int i=1;i<=100;i++){
           int j = 2;
           for(;j<=i/2;j++){
             if(i%j==0) break;
           }
           if(j>i/2)  System.out.print(" "+i);
        }
        long n2 = System.currentTimeMillis();
        System.out.printf("\n循环用时:"+(n2-n1)+"毫秒\n");
        free = runtime.freeMemory();
        System.out.println("Java虚拟机可用空闲内存 "+free+" bytes");
        total=runtime.totalMemory();
        System.out.println("Java虚拟机占用总内存 "+total+" bytes");
    }
}
```

解：略

## 编程题

1. 字符串调用public String toUpperCase()方法返回一个字符串，该字符串把当前字符串中的小写字母变成大写字母；.字符串调用public String toLowerCase()方法返回一个字符串，该字符串把当前字符串中的大写字母变成小写字母。String类的public String concat(String str)方法返回一个字符串，该字符串是把调用该方法的字符串与参数指定的字符串连接。编写一个程序，练习使用这3个方法
1. String类的public char charAt(int index)方法可以得到当前字符串index位置上的一个字符。编写程序使用该方法得到一个字符串中的第一个和最后一个字符
1. 计算某年、某月、某日和某年、某月、某日之间的天数间隔。要求年、月、日使用main方法的参数传递到程序中（见例子4）
1. 编程练习Math类的常用方法
1. 编写程序剔除一个字符串中的全部非数字字符，例如，将形如“ab123you”的非数字字符全部剔除，得到字符串“123”（参看例子10）
1. 使用Scanner类的实例解析字符串："数学87分，物理76分，英语96分"中的考试成绩，并计算出总成绩以及平均分数（参看例子14）
