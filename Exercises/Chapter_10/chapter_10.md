# Chapter10 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编写程序](#编写程序)

<!-- /code_chunk_output -->

## 问答题

1. 如果准备按字节读取一个文件的内容，应当使用`FileInputStream`流还是`FileReader`流? => `FileInputStream`
2. `FileInputStream`流的`read`方法和`FileReader`流的`read`方法有何不同？ => 读取的基本单元不同，`FileInputStream`的基本单元是字节，`FileReader`的基本单元是字符（`FileReader`的字符集使用运行时默认字符集）
3. `BufferedReader`流能直接指向一个文件吗？ => 不能，`BufferedReader`是处理流，需要基于另一个流进行构造
4. 使用`ObjectInputStream`和`ObjectOutputStream`类有哪些注意事项？ => 对象实现了`Serializable`接口
5. 怎样使用输入、输出流克隆对象？ => 将原对象通过输出流写入到的一个节点，再通过输入流从该节点读出构造成新的对象

## 选择题

1. **下列哪个叙述是正确的？**
A．创建`File`对象可能发生异常
B．`BufferedReader`流可以指向`FileInputStream`流
C．`BufferedWriter`流可以指向`FileWriter`流
D．`RandomAccessFile`流一旦指向文件，就会刷新该文件

答：`C`
解：
A => 创建`File`对象时还没有与文件系统进行交互
B => `BufferedReader`不能直接指向字节流
D => 指向文件（创建`RandomAccessFile`流对象）时不会刷新该文件

2. **为了向文件hello.txt尾加数据，下列哪个是正确创建指向hello.txt的流？**
A. `try {  OutputStream out = new FileOutputStream ("hello.txt");
         }
         catch(IOException e){}`
B. `try {  OutputStream out = new FileOutputStream ("hello.txt",true);
         }
    catch(IOException e){}`
C. `try {  OutputStream out = new FileOutputStream ("hello.txt",false);
         }
    catch(IOException e){}`
D. `try {  OutputStream out = new OutputStream ("hello.txt",true);
         }
    catch(IOException e){}`

答：`B`
解：
A => `FileOutputStream(File file)`时`append == false`
D => `OutputStream`是抽象类

## 阅读程序

### 文件E.java的长度是51个字节，请说出E类中标注的【代码1】，【代码2】的输出结果

```java{.line-numbers}
import java.io.*;
public class E {
    public static void main(String args[]) {
        File f = new File("E.java");
        try{
            RandomAccessFile in = new RandomAccessFile(f,"rw");
            System.out.println(f.length());   //【代码1】
            FileOutputStream out = new FileOutputStream(f);
            System.out.println(f.length());  //【代码2】
        }
        catch(IOException e) {
           System.out.println("File read Error"+e);
        }
    }
}
```

答：
`【代码1】=> 51`
`【代码2】=> 0`
解：
`FileOutputStream`构造方法中`append == false`（默认为`false`）时，将清空文件原有内容并指向文件头

### 请说出E类中标注的【代码1】~【代码4】的输出结果

```java{.line-numbers}
import java.io.*;
public class E {
    public static void main(String args[]) {
        int n=-1;
        File f =new File("hello.txt");
        byte [] a="abcd".getBytes();
        try{
            FileOutputStream out=new FileOutputStream(f);
            out.write(a);
            out.close();
            FileInputStream in=new FileInputStream(f);
            byte [] tom= new byte[3];
            int m = in.read(tom,0,3);
            System.out.println(m);       //【代码1】
            String s=new String(tom,0,3);
            System.out.println(s);        //【代码2】
            m = in.read(tom,0,3);
            System.out.println(m);       //【代码3】
            s=new String(tom,0,3);
            System.out.println(s);        //【代码4】
        }
        catch(IOException e) {}
   }
}
```

答：
`【代码1】 => 3`，读取3个字节的长度
`【代码2】 => abc`
`【代码3】 => 1`，文件只剩下1个字节
`【代码4】 => dbc`，读取的1个字节覆盖原来的数据
解：见上面

:warning:此代码运行结果与字符集相关，windows平台默认的`gb2312/gbk`和POSIX平台默认的`utf-8`字符集里英文字符占一个字节，因此，输出该结果

```java{.line-numbers}
//java.lang.String
public byte[] getBytes()
/**Encodes this String into a sequence of bytes \
    using the platform's default charset, \
    storing the result into a new byte array.*/
```

### 了解打印流

我们已经学习了数据流，其特点是用Java的数据类型读写文件，但使用数据流写成的文件用其它文件阅读器无法进行阅读（看上去是乱码）。PrintStream类提供了一个过滤输出流，该输出流能以文本格式显示Java的数据类型。上机实习下列程序：

```java{.line-numbers}
import java.io.*;
public class E {
    public static void main(String args[]) {
        try{
            File file=new File("p.txt");
            FileOutputStream out=new FileOutputStream(file);
            PrintStream ps=new PrintStream(out);
            ps.print(12345.6789);
            ps.println("how are you");
            ps.println(true);
            ps.close();
        }
        catch(IOException e){}
    }
}
```

## 编写程序

1．使用RandomAccessFile流将一个文本文件倒置读出

2．使用Java的输入、输出流将一个文本文件的内容按行读出，每读出一行就顺序添加行号，并写入到另一个文件中。

3．参考例子16，解析一个文件中的价格数据，并计算平均价格，比如该文件的内容如下：
商品列表：
电视机,2567元/台
洗衣机,3562元/台
冰箱,6573元/台
