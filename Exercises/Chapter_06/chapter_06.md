# Chapter06 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编程题（参考例子6）](#编程题参考例子6)

<!-- /code_chunk_output -->

## 问答题

1. 接口中能声明变量吗？ => 不能，只能定义常量
2. 接口中能定义非抽象方法吗？ => 不能
:warning::exclamation:JDK8新增了接口的缺省方法（用`default`修饰）和静态方法（用`static`修饰），详见 https://docs.oracle.com/javase/tutorial/java/IandI/defaultmethods.html
3. 什么叫接口的回调？ => 通过接口引用变量调用实现了该接口的具体类的具体方法实现
4. 接口中的常量可以不指定初值吗？ => 不能，常必须在定义的同时赋初值
5. 可以在接口中只声明常量，不声明抽象方法吗？ => 可以

## 选择题

1. **下列接口中标注的（A,B,C,D）中，哪两个是错误的?**

```java{.line-numbers}
interface Takecare {
   protected void speakHello();          //A
   public abstract static void cry();        //B
   int f();                            //C
   abstract float g();                   //D
}
```

答： `AB`
解：
A ==> 不能为 **`protected`**
B ==> 不能为 **`static`** （JDK8也不能同时用`abstract`和`static`修饰）

2. **将下列（A,B,C,D）哪个代码替换下列程序中的【代码】不会导致编译错误。**
A．public int f(){return 100+M;}
B．int f(){return 100;}
C．public double f(){return 2.6;}。
D．public abstract int f();

```java{.line-numbers}
interface Com {
    int M = 200;
    int f();
}
class ImpCom implements Com {
   【代码】
}
```

答： `A`
解：
B ==> 访问权限不能为 **`default`**
C ==> 返回类型 **`double`** 与 接口的 **`int`** 不同且不是 **`int`** 的子类
D ==> **`abstract method`** 需为 **`abstract class`**

## 阅读程序

### 请说出E类中【代码1】，【代码2】的输出结果

```java{.line-numbers}
interface A {
   double f(double x,double y); 
}
class B implements A {
  public double f(double x,double y) {
     return x*y;
  }
  int g(int a,int b) {
     return a+b;
  }
}
public class E {
public static void main(String args[]) {
      A a = new B();
      System.out.println(a.f(3,5)); //【代码1】
      B b = (B)a;
      System.out.println(b.g(3,5)); //【代码2】
  }
}
```

答：
`【代码1】 => 15.0`
`【代码2】=> 8`

### 请说出E类中【代码1】，【代码2】的输出结果。

```java{.line-numbers}
interface Com {
   int add(int a,int b);
}
abstract class A {
   abstract int add(int a,int b);
}
class B extends A implements Com{
   public int add(int a,int b) {
      return a+b;
   }
}
public class E {
public static void main(String args[]) {
     B b = new B();
     Com com = b;
     System.out.println(com.add(12,6)); //【代码1】
     A a = b;
     System.out.println(a.add(10,5));   //【代码2】
  }
}
```

答：
`【代码1】=> 18`
`【代码2】=> 15`
解： **`class B`** 的 **`public int add(int a,int b)`** 均是对 **`interface Com`** 和 **`abstract class A`** 中的抽象方法的Override，其中对 **`abstract class A`** 的 **`abstract int add(int a,int b);`** 的 **`default`** 访问权限提升为 **`public`** 访问权限，重写的方法在运行时表现出多态

## 编程题（参考例子6）

该题目和第5章习题5的编程题类似，只不过这里要求使用接口而已。

设计一个动物声音“模拟器”，希望模拟器可以模拟许多动物的叫声。要求如下：

1. 编写接口Animal：Animal接口有2个抽象方法cry()和getAnimaName()，即要求实现该接口的各种具体动物类给出自己的叫声和种类名称。
1. 编写模拟器类Simulator：该类有一个playSound(Animal animal)方法，该方法的参数是Animal类型。即参数animal可以调用实现Animal接口类重写的cry()方法播放具体动物的声音、调用重写的getAnimalName()方法显示动物种类的名称。
1. 编写实现Animal接口的Dog类和Cat类：图6.14是Simulator、Animal、Dog、Cat的UML图。
1. 编写主类Application（用户程序）：在主类Application的main方法中至少包含如下代码：
   + Simulator simulator = new Simulator();
   + simulator.playSound(new Dog());
   + simulator.playSound(new Cat());
