# Chapter07 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编写程序](#编写程序)

<!-- /code_chunk_output -->

## 问答题

:warning:《实验指导与习题解答》中对01-03题的答案，“内部类”特指“实例内部类”

1. 内部类的外嵌类的成员变量在内部类中仍然有效吗？ => **实例内部类可以访问外嵌类所有成员变量，静态内部类只能访问外嵌类的静态成员变量**
1. 内部类中的方法也可以调用外嵌类中的方法吗？=> **实例内部类可以访问外嵌类所有成员方法，静态内部类只能访问外嵌类的静态成员方法**
1. 内部类的类体中可以声明类变量和类方法吗？=> **实例内部类不可以，静态内部类可以**
1. 匿名类一定是内部类吗？=> **匿名类是一种内部类**

## 选择题

1. **下列代码标注的（A,B,C,D）中哪一个是错误的？**

```java{.line-numbers}
class OutClass {
    int m = 1;
    static float x;             //A
    class InnerClass {
       int m =12;            //B
       static float n =20.89f;   //C
       InnerClass(){
       }
       void f() {
          m = 100;
       }
    }
    void cry() {
      InnerClass tom = new InnerClass(); //D
    }
}
```

答：`C`
解：实例内部类不能定义静态成员变量

2. **下列哪一个叙述是正确的？**
A．和接口有关的匿名类可以是抽象类。
B．和类有关的匿名类还可以额外地实现某个指定的接口。
C．和类有关的匿名类一定是该类的一个非抽象子类。
D．和接口有关的匿名类的类体中可以有static成员变量。

答：`C`
解：
A => 匿名类均要被实例化，所以不能为抽象类
B => 匿名类没有同时继承类和实现接口的语法，从语法逻辑上看，匿名类必然向上转型到一个引用变量，不存在同时向上转型到一个超类和一个接口的需要
D => 匿名类不能定义 **`static`** 成员

## 阅读程序

### 请说出下列程序的输出结果

```java{.line-numbers}
class Cry {
  public void cry() {
       System.out.println("大家好");
    }
}
public class E {
  public static void main(String args[]) {
       Cry hello=new Cry() {
                    public void  cry() {
                       System.out.println("大家好，祝工作顺利！");
                     }
                };
         hello.cry();
   }
}
```

答：`大家好，祝工作顺利`
解：匿名类重写了 **`cry()`**

### 请说出下列程序的输出结果

```java{.line-numbers}
interface Com{
   public void speak();
}
public class E {
   public static void main(String args[]) {
      Com p=new Com() {
                public void speak() {
                   System.out.println("p是接口变量"); 
                }
            };
      p.speak();
   }
}
```

答：`p是接口变量`
解：匿名类重写了 **`speak()`**

### 请说出下列程序的输出结果

```java{.line-numbers}
import java.io.IOException;
public class E {
   public static void main(String args[]){ 
      try {
            methodA();
      }
      catch(IOException e){
            System.out.print("你好");
            return;  
      }
      finally {
        System.out.println(" fine thanks");
      }
  }
  public static void methodA() throws IOException{
      throw new IOException();
  }
}
```

答：`你好 fine thanks`
解：`finally语句块`在方法返回前被调用

### 实习下列程序，了解静态内部类

```java{.line-numbers}
class RedCowForm {
     static class RedCow {//静态内部类是外嵌类中的一种静态数据类型
      void speak() {
         System.out.println("我是红牛");
      }
   }
}
class BlackCowForm {
   public static void main(String args[]) {
      RedCowForm.RedCow red =
         new RedCowForm.RedCow();//如果RedCom不是静态内部类，此代码非法
      red.speak();
   }
}
```

## 编写程序

第3章例子9的程序允许用户在键盘依次输入若干个数字（每输入一个数字都需要按回车键确认），程序将计算出这些数的和以及平均值。请在第3章的例子9中增加断言语句，当用户输入的数字大于100或小于0时，程序立刻终止执行，并提示这是一个非法的成绩数据。
