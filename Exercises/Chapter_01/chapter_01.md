# Chapter01 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)

<!-- /code_chunk_output -->

## 问答题

1. Java语言的主要贡献者是谁？ => James Gosling(father of java)
2. 开发Java应用程序需要经过哪些主要步骤？ => code, compile, run
3. Java源文件是由什么组成的？一个源文件中必须要有public类吗？ => Java源文件由`package`语句、`import`语句、类或接口组成（所有的属性和方法都必须在类或接口中），Java应用程序必须有至少一个`main`入口方法，一个源文件不一定必须有`public`类
4. 如果JDK的安装目录为D:\jdk，应当怎样设置path和classpath的值？ => `Path=D:\jdk\bin;%Path%`， `CLASSPATH=.;D:\jdk\jre\lib\rt.jar`(rt is short of **R**un**T**ime)
5. Java源文件的扩展名是什么？Java字节码的扩展名是什么？ => `.java`，`.class`
6. 如果Java应用程序主类的名字是Bird，编译之后，应当怎样运行该程序？ => `java Bird`
7. 有哪两种编程风格，在格式上各有怎样的特点？ => 独行风格和行尾风格

## 选择题

1. **下列哪个是JDK提供的编译器？**
A. java.exe
B. javac.exe
C. javap.exe
D. javaw.exe

答：`B`
解：
A => java - launch a Java application
B => javac(short of **C**ompile) - read Java class and interface definitions and compile them into bytecode and class files
C => javap - disassemble one or more class files
D => javaw(Windows Platfrom Only) - launch a Java application without a console window

>see more: https://docs.oracle.com/en/java/javase/13/docs/specs/man/index.html

2. **下列哪个是Java应用程序主类中正确的main方法？**
A. public void main (String args[])
B. static void main (String args[])
C. public static void Main (String args[])
D. public static void main (String args[])

答：`D`
解：
C => Main中M大写

## 阅读程序

### 阅读下列Java源文件，并回答问题

```java{.line-numbers}
public class Person {
   void speakHello() {
      System.out.print("您好，很高兴认识您");
      System.out.println(" nice to meet you");
   }
}
class Xiti {
   public static void main(String args[]) {
      Person zhang = new Person();
      zhang.speakHello();
   }
}
```

（a）上述源文件的名字是什么？ => `Person.java`（源文件名须与`public class`类名一致）
（b）编译上述源文件将生成几个字节码文件？这些字节码文件的名字都是什么？=> 两个（每个类一个字节码文件），`Person.class`和`Xiti.class`
（c）在命令行执行java Person得到怎样的错误提示？执行java xiti得到怎样的错误提示？执行java Xiti.class得到怎样的错误提示？执行java Xiti得到怎样的输出结果？

+ `java Person` => `Error: main method not found in class Person, please define the main method as:public static void main(String[] args)`
+ `java Xiti.class` => `Error: Could not find or load main class Xiti.class`（java运行字节码时不能加后缀，而是直接运行主类名，可能的原因是文件名与后缀以`.`分隔，而包名之间的分隔符也是`.`）
+ `java Xiti` => `您好，很高兴认识您 nice to meet you`

:warning::exclamation:**《实验指导与习题解答》中对于（c）的参考答案有误**
