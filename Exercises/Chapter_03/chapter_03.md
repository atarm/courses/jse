# Chapter03 Textbook Exercises

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [问答题](#问答题)
2. [选择题](#选择题)
3. [阅读程序](#阅读程序)
4. [编程序题](#编程序题)

<!-- /code_chunk_output -->

## 问答题

1. 关系运算符的运算结果是怎样的数据类型？ => `boolean`（注意：与C不一样）
2. if语句中的条件表达式的值是否可以是int型？ => 不可以
3. while语句中的条件表达式的值是什么类型？ => `boolean`
4. switch语句中必须有default选项码？ => 非必须
5. 在while语句的循环体中，执行break语句的效果是什么？ => 跳出本循环体
6. 可以用for语句代替while语句的作用吗？ => 可以（不一定能代替`do-while`）

## 选择题

1. **下列哪个叙述是正确的？**
A. 5.0/2+10的结果是double型数据
B. (int)5.8+1.0的结果是int型数据
C. '苹'+ '果'的结果是char型数据
D. (short)10+'a'的结果是short型数据

答：`A`
解：
A => `5.0`是`double`，故结果是`double`
B => `1.0`是`double`，故结果是`double`
C => 提升至`int`，结果是`int`
D => 提升至`int`，结果是`int`

>Java数值变量运算表达式自动类型转换规则
>数值型数据的转换：`byte/short/char --无条件转换--> int --有条件转换--> long --有条件转换--> float --有条件转换--> double`

2. **用下列哪个代码替换程序标注的【代码】会导致编译错误？**
A．m-->0
B．m++>0
C．m = 0
D．m>100&&true

```java{.line-numbers}
public class E{
   public static void main (String args[ ]) {
      int m=10,n=0;
      while(【代码】) {
         n++;
      }
   }
}
```

答：`C`
解：
C => `m = 0`表达式的值为`0`（即先将`0`赋值给`m`，然后将`m`的值作为表达式的值），`0`是一个`int`而不是`boolean`

3．**假设有`int x=1;`以下哪个代码导致“可能损失精度，找到int需要char”这样的编译错误。**
A．short t = 12+'a';
B．char c = 'a'+1;
C．char m = 'a'+x;
D．byte n = 'a'+1;

答：`C`

解：:warning::book:P34：Java允许把不超出`byte`、`short`和`char`的取值范围的算术表达式的值赋给`byte`、`short`和`char`变量 => **适用于编译时可确定值的情况，即仅有常量或字面量参与的算术表达式，有变量参与的算术表达式按类型转换规则执行**

即，如果改成`final int x=1;`，则`char m='a'+x;`无编译错误

## 阅读程序

### 下列程序的输出结果是什么？

```java{.line-numbers}
public class E {  
   public static void main (String args[ ]){
      char x='你',y='e',z='吃';
      if(x>'A'){
         y='苹';
         z='果';
      }
      else
         y='酸';
      z='甜';
      System.out.println(x+","+y+","+z);
   }
}
```

答：`你,苹,甜`

解：

1. 中文字符的值大于英文字符，因此执行`if`语句
1. `else`语句没有`{}`，因此`else`只包括`y='酸';`语句
1. `x+","+y+","+z`中`","`是字符串，因此执行的是字符串拼接运算，而不是整型算术运算

### 下列程序的输出结果是什么？

```java{.line-numbers}
public class E {
   public static void main (String args[ ]) {
      char c = '\0';
      for(int i=1;i<=4;i++) {
         switch(i) {
            case 1:  c = 'J';
                     System.out.print(c);  
            case 2:  c = 'e';
                     System.out.print(c);
                     break;
            case 3:  c = 'p';
                     System.out.print(c);
            default: System.out.print("好");
         }
      }
   }
}
```

答：`Jeep好好`
解：

1. `for`循环第一次： `case 1`语句没有`break` => **输出`Je`**
1. `for`循环第二次： `case 2`语句 => **输出`e`**
1. `for`循环第三次： `case 3`语句没有`break` => **输出`p好`**
1. `for`循环第四次： `default`语句 => **输出`好`**

### 下列程序的输出结果是什么？

```java{.line-numbers}
public class E {
   public static void main (String []args)   {
      int x = 1,y = 6;
      while (y-->0) {
          x--;
      }
      System.out.print("x="+x+",y="+y);
   }
}
```

答：`x=-5,y=-1`
解：

1. 当`y`的值为1时，`y--`表达式的值为`1`然后自减到`0`，此时`y-->0`为`true`，故执行循环体
1. 当`y`的值为0时，`y--`表达式的值为`0`然后自减到`-1`，此时`y-->0`为`false`，故不执行循环体

因此，`y`自减了7次，循环体执行了6次，即，`x`自减了6次，`x`的最终值为`1 - 6 => -5`，`y`的最终值为`6 - 7 => -1`

## 编程序题

1．编写应用程序求1!+2!+…+10!。
2．编写一个应用程序求100以内的全部素数。
3．分别用do-while和for循环计算1+1/2!+1/3!+1/4!…  … 的前20项和。
4．一个数如果恰好等于它的因子之和，这个数就称为“完数”。编写应用程序求1000之内的所有完数。
5．编写应用程序，使用for循环语句计算8+88+888…前10项之和。
6．编写应用程序，输出满足1+2+3…+n<8888的最大正整数n。
