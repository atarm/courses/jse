---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第08章：常用实用类

<!-- slide -->
# 本章主要内容

1. 字符串存储：String, StringBuffer
1. 字符串处理：StringTokenizer, Scanner
1. 时间日期处理：Date, Calendar
1. 数学、数字与伪随机：Math, BigInteger和Random
1. 数字格式化（格式到字符串）：String.format
1. RTTI机制：Class
1. 命令行对象：Console
1. 正规表达式（字符串处理）：Pattern, Match

<!-- slide -->
# 本章重点和难点

1. 重点：
    1. 字符串的常用方法
    1. 字符串分析器使用
    1. 类Date和类Calendar以及类Math的使用
1. 难点：
    1. 字符串分析器的使用
    1. 各常用类的实际运用

<!-- slide -->
# 8.1：`String`:book:P175

```java{.line-numbers}
package java.lang;

public final class String extends Object
    implements Serializable, Comparable<String>, CharSequence

The String class represents character strings.
    All string literals in Java programs, such as "abc",
    are implemented as instances of this class.
Strings are constant;
    their values cannot be changed after they are created.
    String buffers support mutable strings.
    Because String objects are immutable they can be shared.
```

<!-- slide -->
# `String`的特点

1. 可直接使用： **`String`** 在 **`java.lang`** 中
1. 内容不可变： **`String`** 对象创建之后，内容不可变（引用变量的值可变）
1. 不能有子类： **`String`** 是 **`final`** 类

<!-- slide -->
# 8.1.1：构造字符串对象:book:P175

```java{.line-numbers}
//字面量对象赋值
String s01="123";
//字面量对象构造
String s02=new String("123");
//String对象构造
String s03=new String(s02);
//字符数组构造
String s04=new String({'中','文'});
```

<!-- slide -->
# `public String(byte[] bytes)`

1. Constructs a new String by decoding the specified array of bytes **using the platform's default charset**.
1. The length of the new String is a function of the charset, and hence may not be equal to the length of the byte array.
1. The behavior of this constructor when the given bytes are not valid in the default charset is unspecified. The CharsetDecoder class should be used when more control over the decoding process is required.

## :point_right:`public String(byte[] bytes, Charset charset)`

<!-- slide -->
# 8.1.2：字符串的并置:book:P177

1. **`String`** 对象用“+”进行并置运算将生成一个 **新的`String`对象**
1. 字面量并置（编译时可确定）： 如果是两个常量进行并置运算，那么得到的仍然是常量，如果常量池没有这个常量就放入常量池（编译期常量折叠）
1. 引用变量并置（编译时不可定）：只要有一个是引用变量，那么Java就会在动态区存放所得到的新String对象的实体和引用

<!-- slide -->
# 例8.1

```java{.line-numbers}
String hello = "你好";
String testOne = "你"+"好";//编译时可确定
System.out.println(hello == testOne); //输出结果是true
System.out.println("你好" == testOne); //输出结果是true
System.out.println("你好" == hello); //输出结果是true
String you = "你";
String hi = "好";
String testTwo = you+hi;//编译时不可定
System.out.println(hello == testTwo); //输出结果是false
String testThree = you+hi;
System.out.println(testTwo == testThree); //输出结果是false
```

<!-- slide -->
# 8.1.2：String类的常用方法:book:P178

```java{.line-numbers}
public int length()
public boolean equals(String s)
public boolean startsWith(String s)
public int compareTo(String s)
public boolean contains(String s)
public int indexOf (String str)
public String substring(int startPoint)
public String trim()
```

<!-- slide -->
# 8.1.3：字符串与基本数据的相互转化:book:P182

<!-- slide -->
# 字符串解码到基本数据类型

```java{.line-numbers}
public static int parseInt(String s) throws NumberFormatException
public static byte parseByte(String s) throws NumberFormatException
public static short parseShort(String s) throws NumberFormatException
public static long parseLong(String s) throws NumberFormatException
public static float parseFloat(String s) throws NumberFormatException
public static double parseDouble(String s) throws NumberFormatException
```

<!-- slide -->
# 基本数据类型编码成字符串

```java{.line-numbers}
public static String valueOf(byte n)
public static String valueOf(int n)
public static String valueOf(long n)  
public static String valueOf(float n)
public static String valueOf(double n)
```

<!-- slide -->
# 8.1.5：对象的字符串表示:book:P183

```java{.line-numbers}
public String toString()

In other words, this method returns a string equal to the value of:
getClass().getName() + '@' + Integer.toHexString(hashCode())
```

<!-- slide -->
# 8.1.6：字符串与字符、字节数组:book:P184

<!-- slide -->
# 字符串与字符数组

```java{.line-numbers}
String(char[] value)
String(char[] value, int offset, int length)
public void getChars(int srcBegin, int srcEnd, char[] dst, int dstBegin)
public char[] toCharArray()
```

<!-- slide -->
# 字符串与字节数组

```java{.line-numbers}
String(byte[] bytes)
String(byte[] bytes, Charset charset)
String(byte[] bytes, int offset, int length)
String(byte[] bytes, int offset, int length, Charset charset)
public byte[] getBytes()
public byte[] getBytes(String charsetName)
```

<!-- slide -->
# 字符串的加密算法

1. 得到密钥的字符数组pArray
1. 得到待加密字符串的字符数组sArray
1. 对sArray的每个字符按pArray的长度分组后对应字符进行加法运算

## 例8_8

<!-- slide -->
# 8.1.7：正则表达式及字符串的替换与分解:book:P186

>正则表达式（英语：Regular Expression，常简写为regex、regexp或RE），又称正则表示式、正则表示法、规则表达式、常规表示法，是计算机科学的一个概念。正则表达式使用单个字符串来描述、匹配 **一系列符合某个句法规则的字符串**。在很多文本编辑器里，正则表达式通常被用来检索、替换那些符合某个模式的文本。
>
>许多程序设计语言都支持利用正则表达式进行字符串操作。例如，在Perl中就内建了一个功能强大的正则表达式引擎。正则表达式这个概念最初是由Unix中的工具软件（例如sed和grep）普及开的。

<!-- slide -->
# Java中的正则表达式

1. 规则表示： 包含表示规则的 **`String`**
1. 元字符： 预定义的规则
1. 字符集合： 使用方括号`[]`
1. 量词： `?`、`*`、`+`、`{n}`、`{n,}`、`{n,m}`

<!-- slide -->
# 字符串的替换

```java{.line-numbers}
public String replaceAll(String regex,String replacement)

//str will be "12你好567你好"
String str ="12hello567bird".replaceAll("[a-zA-Z]+","你好");
```

<!-- slide -->
# 字符串的分解

```java{.line-numbers}
public String[] split(String regex)

String str = "1949年10月1日是中华人民共和国成立的日子";
String regex="\\D+";
//digitWord[] will be {"1949","10","1"}
String digitWord[]=str.split(regex);

str = "公元1949年10月1日是中华人民共和国成立的日子";
//digitWord[] will be {""."1949","10","1"}
digitWord[]=str.split(regex);
```

<!-- slide -->
# 8. 2：`StringTokenizer`:book:P191

```java{.line-numbers}
public class StringTokenizer
    extends Object
    implements Enumeration<Object>

/*uses the default delimiter set, which is space and "\t\n\r\f"*/
StringTokenizer(String s)
/*each character in delim can be arranged in any order and any times*/
StringTokenizer(String s, String delim)

nextToken()
hasMoreTokens()
countTokens()
```

<!-- slide -->
# `Interface Enumeration<E>`

```java{.line-numbers}
boolean hasMoreElements()
E nextElement()
```

<!-- slide -->
# 8. 3：`Scanner`:book:P192

```java{.line-numbers}
public final class Scanner
    extends Object
    implements Iterator<String>, Closeable

public Scanner(String source)

public Scanner useDelimiter(Pattern pattern)
public Scanner useDelimiter(String pattern)
```

<!-- slide -->
# Scanner的解析步骤

```java{.line-numbers}
/**
An iterator over a collection. Iterator takes the place of Enumeration \
    in the Java Collections Framework.\
    Iterators differ from enumerations in two ways:
        1. Iterators allow the caller to remove elements from \
        the underlying collection during the iteration with well-defined semantics.
        2. Method names have been improved.
*/
Interface Iterator<E>

boolean hasNext()
E next()
```

<!-- slide -->
# 8.4  `StringBuffer`

## **`String`** 的字符不可修改，**`StringBuffer`** 的字符可修改

```java{.line-numbers}
public final class StringBuffer
    extends Object
    implements Serializable, CharSequence

public StringBuffer()
public StringBuffer(int capacity)
public StringBuffer(String str)
public StringBuffer(CharSequence seq)
```

<!-- slide -->
# `StringBuffer`的常用方法

```java{.line-numbers}
public StringBuffer append(XXX)
public chat charAt(int n)
public void setCharAt(int n ,char ch)
public StringBuffer insert(int index, String str)
public StringBuffer reverse()
public StringBuffer delete(int startIndex, int endIndex)
public deleteCharAt(int index)
public StringBuffer replace(int startIndex,int endIndex,String str)
```

<!-- slide -->
# `Date`

```java{.line-numbers}
Date()
Date(long time)
public long System.currentTimeMillis()
```

<!-- slide -->
# `Calendar`

```java{.line-numbers}
Calendar.getInstance()

public final void set(int year,int month,int date)
public final void set(int year,int month,int date,int hour,int minute)
public final void set(int year,int month, int date, int hour, int minute,int second)

public long getTimeInMillis()
public final void setTime(Date date)
public int get(int field)//field defined in Calendar as public static int
```

<!-- slide -->
# 8.6：日期的格式化

```java{.line-numbers}
public static String format(String format,
                            Object... args)

public static String format(Locale l,
                            String format,
                            Object... args)
```

<!-- slide -->
# 8. 7：`Math`、`BigInteger`和`Random`

<!-- slide -->
# `Math`

```java{.line-numbers}
//class static field
E = 2.7182828284590452354
PI = 3.14159265358979323846

//class static method
public static long abs(double a)
public static double max(double a,double b)
public static double min(double a,double b)
public static double random()
public static double pow(double a,double b)
public static double sqrt(double a)
public static double log(double a)
public static double sin(double a)
public static double asin(double a)
```

<!-- slide -->
# `BigInteger`

```java{.line-numbers}
//constructor
BigInteger(String val)

//instance method
public BigInteger add(BigInteger val)
public BigInteger subtract(BigInteger val)
public BigInteger multiply(BigInteger val)
public BigInteger divide(BigInteger val)
public BigInteger remainder(BigInteger val)
public int compareTo(BigInteger val)
public BigInteger pow(int a)
public String toString()
public String toString(int p)
```

<!-- slide -->
# `Random`

```java{.line-numberso}

/*return [0,1)*/
public static double Math.random()

//constructor
public Random()
public Random(long seed)

//method
nextXXX()
```

<!-- slide -->
# 8. 8：数字格式化:book:P206

1. 数字格式化： 按照指定格式得到的字符序列
1. `format(格式化模式,值列表...)`
    1. 格式化模式： 用双引号括起的字符序列(字符串)，该字符序列中由 **格式符** 和 **普通字符** 组成
        1. **格式符：** **`%[argument_index$][flags][width][.precision]conversion`**
    1. 值列表： format方法中的“值列表”是用逗号分隔的变量、常量或表达式
    1. 格式化顺序： 默认按从左到右的顺序使用“格式化模式”中的格式符来格式化“值列表”中对应的值，而“格式化模式”中的普通字符保留原样

<!-- slide -->
# 格式化整数

1. **conversion：** **`%d`**，**`%o`**，**`%x`** 和 **`%X`**
1. **flags：**
    1. **`+`** 正数时，强制添加上正号
    1. **`,`** 整数部分，按"千"分组
    1. **`0`** 不足位时，左边增加数字 **`0`**
1. **width：**
    1. **`m`** 不足m位时，左边增加空格
    1. **`-m`** 不足m位时，右边增加空格

<!-- slide -->
# 格式化浮点数

1. **conversion：** **`%f`**，**`%e(%E)`**，**`%g(%G)`** 和 **`%a(%A)`**
1. **flags：**
    1. **`+`** 正数时，强制添加上正号
    1. **`,`** 整数部分，按"千"分组
    1. **`0`** 不足位时，左边增加数字 **`0`**
1. **width：** 位数包括小数点和小数位数
    1. **`m`** 不足m位时，左边增加空格
    1. **`-m`** 不足m位时，右边增加空格
1. **.precision：** **`.n`** 保留n位小数

<!-- slide -->
# 8.9： `Class`与`Console`:book:P211

```java{.line-numbers}
//get class
public static Class<?> forName(String className) throws ClassNotFoundException

//get instance
public T newInstance()
              throws InstantiationException, IllegalAccessException

//System.console(), get system console
public static Console console()
public char[] readPassword()
```

<!-- slide -->
# 8.10：`Pattern`与`Matcher`:book:P212

```java{.line-numbers}
public static Pattern compile(String regex, int flags)
public Matcher matcher(CharSequence input)

public interface CharSequence
    All Known Implementing Classes:
    CharBuffer, Segment, String, StringBuffer, StringBuilder
```

<!-- slide -->
# `Matcher`

```java{.line-numbers}
public boolean find()
public boolean find(int start)
public boolean matches()
public boolean lookingAt()

public String replaceAll(String replacement)
public String replaceFirst(String replacement)
```

<!-- slide -->
# 8.11：应用举例:book:P214

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
