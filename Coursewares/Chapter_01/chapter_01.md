---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第01章：Java入门

<!-- slide -->
# 主要内容

1. Java简介
1. 安装JDK
1. 简单的Java应用程序
1. 编程风格与注释

<!-- slide -->
# Java是什么

## Java之父James Gosling

1. Java是一种编程语言，也是一个完备的计算平台Platform
1. Java的前身是Oak，SUN公司“Green Project”中编写Star7应用程序的编程语言
1. Oak已被注册，工程师们边喝咖啡边讨论，起名Java
1. Java与Javascript没有关系

## Java是Indonesia的一个盛产咖啡的岛屿

<!-- slide -->
# Java最初的应用场景

1. 为移动设备编写程序J2ME
1. 为网页客户端编写交互程序Applet

<!-- slide -->
# Java当前的应用场景

1. Android
1. Enterprise && Distributed

## 有心栽花花不开，无心插柳柳成荫

## 胖客户端GUI不是Java的强项（资源占用高、非OS原生界面）

<!-- slide -->
# 教材章节路线

```dot
digraph learn_java{
    bgcolor=transparent;
    rankdir=LR;
    compound=true;

    node[shape=box];

    chapter01;

    subgraph cluster_basic_syntax{
        label="基础语法";
        chapter02;chapter03;
        //rank = same;
    }

    subgraph cluster_core{
        label="OO核心";
        chapter04;
        chapter05;
        chapter06;
        chapter07;
    }

    subgraph cluster_app_basic{
        label="基础工具";
        chapter08;
        chapter09;
    }

    subgraph cluster_special{
        label="专项应用";
        chapter10;
        chapter11;
        chapter12->chapter13;
        chapter14;
        chapter15;
    }

    chapter01->chapter02[lhead=cluster_basic_syntax];
    chapter02->chapter04[ltail=cluster_basic_syntax,lhead=cluster_core];
    chapter04->chapter08[ltail=cluster_core,lhead=cluster_app_basic];
    chapter08->chapter10[ltail=cluster_app_basic,lhead=cluster_special];
}
```

<!-- slide -->
# 前导课程与后继课程

```dot
digraph leading_and_succeeding{
    bgcolor=transparent;
    rankdir=LR;

    node[shape=box];

    C->Java;
    database->Java->design_pattern[dir=both,style=dashed,minlen=3];
    {
        Java,design_pattern,database;
        rank=same;
    }
    Java->{JavaEE,Android};
}
```

<!-- slide -->
# 1.1：Java的地位:book:P1

1. 网络地位
1. 语言地位
1. 需求地位

<!-- slide -->
# Java的特点

1. 相对简单==>无处不在的 **设计模式** 和 **指针**（隐藏指针运算）
1. 面向对象
1. 平台无关（自成平台）==>Java诞生时都是功能手机
1. 内置多线程
1. 全动态链接

:point_right: 完备的计算平台，自成平台，自成生态，向多范式转变

<!-- slide -->
# Java版本历史

1. **JDK 1.0 (January 23, 1996)**
1. JDK 1.1 (February 19, 1997)
1. J2SE 1.2/Java2 (December 8, 1998)
1. J2SE 1.3 (May 8, 2000)
1. **J2SE 1.4 (February 6, 2002)**
1. **J2SE 1.5/J2SE 5.0 (September 30, 2004)**
1. Java SE 6 (December 11, 2006)

<!-- slide -->
# Java版本历史（续）

## 2010年Oracle收购SUN

1. Java SE 7 (July 28, 2011)
1. **Java SE 8 (March 18, 2014)**
1. Java SE 9 (September 21, 2017)
1. Java SE 10 (March 20, 2018)
1. Java SE 11 (September 25, 2018)
1. **Java SE 12 (March 19, 2019)**

<!-- slide -->
# JVM Java Virtual Machine

1. JVM是Java程序运行的平台
1. JVM负责Java程序的各种资源管理
1. Java程序编译后生成Java字节码（JVM的目标代码/中间代码），运行时再将Java字节码转换成具体运行平台的目标代码

<!-- slide -->
# 1.3：安装JDK:book:P5

<!-- slide -->
# Java三大平台

## 1999年，Java One大会上，SUN公司公布Java体系架构

1. J2ME（Java 2 Platform, Micro Edition）==> JavaME
1. J2SE（Java 2 Platform, Standard Edition）==> JavaSE
1. J2EE（Java 2 Platform, Enterprise Edition）==> JavaEE

<!-- slide -->
# JCP与JSR

## Java是一套由JCP组织制定的JSR标准规范

## 厂商或组织根据JSR实现具体的Java产品

1. **JCP：** Java Community Process
1. **JCP EC：** JCP Executive Committee
1. **JSR：** Java Specification Requests
1. **RI：** Reference Implementation
1. **TCK：** Technology Compatibility Kit

<!-- slide -->
# JRE与JDK

1. **JRE：** Java Runtime Environment
1. **JDK：** Java Development Kit

<!-- slide -->
# Open JDK与Oracle JDK

## Open JDK和Oracle JDK（原SUN JDK）都是JDK的RI

## 2006年，SUN将Java开源，即Open JDK，大部分RI派生自Open JDK（包括Oracle JDK），各种JDK采用不同的许可证

## 通过TCK兼容性测试的实现才可以使用Java这一商标

<!-- slide -->
# `path`和`classpath`

1. `path`： `OS`寻找可执行文件的路径
1. `classpath`：`JVM`寻找`class`的路径

`JDK5以上版本可以不设置环境变量`:question:

<!-- slide -->
# 1.4：Java程序的开发步骤:book:P8

1. 编写源文件：扩展名必须是`.java`
1. 编译Java源程序：用Java编译器（`javac.exe`）编译源文件，得到`.class`
1. 运行Java程序：使用Java解释器/JVM（`java.exe`）解释执行`.class`

<!-- slide -->
# 1.5：简单的Java应用程序:book:P9

```java{.line-numbers}
public class Hello {
    public static void main (String args[]) {
        System.out.println("大家好!");
        System.out.println("Nice to meet you");
        Student stu = new Student();
        stu.speak("We are students");
   }
}
class Student {
    public void speak(String s) {
        System.out.println(s);
   }
}
```

<!-- slide -->
# 记事本程序引起的UTF-8文件问题

`windows记事本`另存为`utf-8编码`会在文件开头加上`EF BB BF`三个字节（即BOM）

<!-- slide -->
# 没有银弹

:book:P11：字节码的兼容性

<!-- slide -->
# 1.5.3：运行

## 一个Java应用程序必须有一个类含有`public static void main(String args[])`入口方法，称这个类是应用程序的主类

<!-- slide -->
# 1.6：Java反编译

## `javap.exe`

<!-- slide -->
# 1.7：编程风格

1. Allmans风格： “独行”风格，即左、右大括号各自独占一行
1. Kernighan风格： “行尾”风格，即左大括号在上一行的行尾，而右大括号独占一行

<!-- slide -->
# 1.7.3 注释

1. 单行注释：`//`
1. 多行注释：`/*...*/`
1. 文档注释：`/**...*/`（可通过`javadoc`生成帮助文档）

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok: