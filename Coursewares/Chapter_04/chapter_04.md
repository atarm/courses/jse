---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第04章：类与对象

<!-- slide -->
# 本章主要内容

1. 类与对象
1. 构造方法与对象的创建
1. 类与程序的基本结构
1. 参数传值
1. 对象的组合、类之间的6种关系
1. 实例成员与类成员
1. 方法重载
1. this和super关键字
1. package与import语句
1. 对象数组

<!-- slide -->
# 本章重点和难点

1. OO语法、类成员和实例成员
1. 构造方法与对象的创建过程，引用变量与实体（对象）的关系
1. **`package`** 与 **`import`**
1. 类之间的6种关系、4种访问权限

<!-- slide -->
# 4.1：（命令式）编程语言的几个发展阶段:book:P50

1. 面向机器语言
1. 面向过程语言
1. 面向对象语言

## 面向对象语言的三个特征：封装（Encapsulation）、继承（Inheritance）和多态（Polymorphism）:book:P51

<!-- slide -->
# 理念的改变==>抽象维度的不同

1. 面向过程的函数没有明确的调用主体，面向对象的方法有明确的调用主体（即：对象）
1. 使用面向对象语言时，**需要完成某种任务时，首先要想到，谁来完成任务，即哪个对象去完成任务；提到数据，首先想到这个数据属于哪个对象**

<!-- slide -->
# 4.2：类:book:P52

1. Java的源程序由若干个源文件组成，每个源文件由若干个类组成
1. **`class`** 和 **`interface`** 是Java中的引用型数据类型，用引用型数据类型定义的变量称为 **“引用变量”**，引用变量引用（即指向）的实体称为 **“对象”**，即类是用来创建对象的模板
1. 类的实现（定义）包括：类声明和类体（定义）

## :warning::book:P52“类的变量称作对象变量，简称对象”==>描述不准确

<!-- slide -->
# 4.2.1：类声明

1. 类：封装 **数据** 和 **行为** 用于表征一种实体
1. Java类的命名规范：
    1. **字母使用英文**
    1. 每个单词首字母大写（包括首单词的首字母）
    1. 容易识别、见名知意

<!-- slide -->
# 4.2.2：类体

1. 类声明之后的一对大括号“{”，“}”以及它们之间的内容称作类体
1. 类体的内容由两部分构：一部分是变量的声明，用来刻画属性；另一部分是方法的定义，用来刻画行为功能

## :warning:成员变量可在定义的同时初始化、但不能定义后在类体内赋值:book:P57

## :warning:类定义之后没有 **`;`**

```java{.line-numbers}
class Lader{
    float abover;//semicolon here
    abover = 1.1f;//error
    float computerArea(){...}//no semicolon here
}//no semicolon here
```

<!-- slide -->
# 4.2.3：成员变量与局部变量

1. 成员变量：类中定义的变量，**有默认初始化值**，类体内均有效（**与定义位置无关**）
1. 局部变量：方法中定义的变量，**无默认初始化值**，**定义之后所在语句块内** 有效
1. 如果局部变量的名字与成员变量的名字相同，则成员变量被隐藏，即该成员变量在这个方法内无法直接访问（隐式访问），使用 **`this`** 显式访问（:book:P84）

```java{.line-numbers}
if(m > 9){//P59
    int z = 10;
}
```

## :warning:形参也是局部变量

<!-- slide -->
# 例题:book:P54

```java{.line-numbers}
class Lader{
    float above,area;
    float computerArea(){
        area=(above+bottom)*height/2.0f;
        return area;
    }
    float bottom;//define here
    void setHeight(float h){
        height=h;
    }
    float height;//define here
}
```

<!-- slide -->
# 4.2.4：方法

```java{.line-numbers}
<ret_type> <method_name>(<parameters>){
    <method_body>;
}
```

<!-- slide -->
# 4.2.6：类的UML图:book:P57

## :point_right:当方法不需要返回数据时，其返回类型应定义为`void`

<!-- slide -->
# 变量和方法的命名规则

1. **字母使用英文**
1. 除首单词的首字母小写外，其他单词首字母大写
1. 容易识别、见名知意

<!-- slide -->
# 4.3：构造方法与对象的创建:book:P57

<!-- slide -->
# 4.3.1：构造方法

## 构造方法用于创建对象后的初始化

1. 构造方法是一种特殊方法，**与类名同名**，**没有返回类型（`void`也不行）**
1. 未显式定义构造方法时，**系统默认提供一个无参的、空的缺省（默认）构造方法**，显式定义一个及以上的构造方法时，**该缺省构造方法即被覆盖（不提供）**，此时，建议 **显式定义** 一个无参的缺省构造方法

```java{.line-numbers}
class Point{
    Point(){}//a explicit defined default constructor
    Point(int a,int b){}//a constructor
    void Point(int a,int b){}//not a constructor
}
```

<!-- slide -->
# 4.3.2：创建对象

1. 定义了引用变量后，需指向具体的对象（创建或将已存在对象赋值给引用变量）
1. 使用 **`new`** 和 **构造方法** 创建和初始化对象

```java{.line-numbers}
Point p1 = new Point();
Point p2 = new Point(100,200);
```

<!-- slide -->
# 对象的内存模型:book:P60

1. :warning:教材中关于引用变量指向对象的示意图有误（图4.4、图4.5等），应表示为引用变量指向对象的首地址
1. :point_right: 如需深入了解面向对象的内存模型，可参阅[《深入探索C++对象模型》](https://book.douban.com/subject/1091086/)
1. :point_right: 引用变量存储在栈内存中，对象存储在堆内存中

<!-- slide -->
# 4.3.3：使用对象

通过 **点运算符`.`** 访问成员

<!-- slide -->
# 4.3.4：对象的引用和对象本身（对象实体）

1. 多个引用变量，如果具有相同的引用（指向相同的内存空间），那么就具有完全相同的实体
1. 使用 **`==`** 比较两个引用变量时，比较的是内存地址，而不是对象的值
1. 比较对象的值是否相等，首先应 **重写** 类的 **`equals(Object obj)`** 方法（继承自 **`Object`**），然后使用该方法进行比较对象的值是否相等

<!-- slide -->
# 垃圾回收:book:P64

1. JVM自动回收不再被任何变量引用的对象，即`GC(Garbage Collector)`
1. :warning:调用`System.gc()`并 **不一定会** 进行GC操作

```java{.line-numbers}
When control returns from the method call, \
    the virtual machine has made its best effort to \
    recycle all discarded objects.
```

<!-- slide -->
# 4.4：类与程序的基本结构

@import "./00_Diagrams/structure_of_java_app.gv"

<!-- slide -->
# 4.4：类与程序的基本结构（续）

1. Java应用程序有一个主类（即含有`main()`方法的类）
1. 建议一个源文件只包含一个类

<!-- slide -->
# 4.5：参数传值:book:P68

1. Java只有值传递（传递参数值的复本）
1. 传递值类型时，**实参精度应不高于形参精度** （即可以进行隐式类型转换）
1. 传递引用类型时，**实参应该为形参的同类型或子类型** （即可以向上转型）

:warning:引用类型传递地址的复本（与C传递指针一样），而不是对象的复本

<!-- slide -->
# 4.5.4：可变参数（可变形参）:book:P70

1. 可变参数指形参个数延迟到调用时由实参个数决定
1. 可变参数的类型必须相同
1. 可变参数的参数代表必须是形参列表的最后一个

:point_right: **可变参数列表** 将被包装成 **数组名** 为 **参数代表** 的 **`Array`**

<!-- slide -->
# 可变参数:book:P71

```java{.line-numbers}
public int getSum(int ...x){
    int sum = 0;
    for(int parma : x){
        sum + = param;
    }
    return sum;
}
```

<!-- slide -->
# 4.6：对象的组合

1. “黑盒”复用：当前对象无需知晓所包含对象的细节
1. 弱耦合关系：随时更换所包含的（具体）对象
1. 将其他引用类型作为成员变量可表示 **关联（has-a）** 、 **聚合（contains-a）** 或 **组合（owns-a）** 的关系

<!-- slide -->
# 4.6.2：关联关系和依赖关系的UML图

<!-- slide -->
# 类之间的6种关系

|-|代码表现|语义|备注|
|--|--|--|--|
|泛化|继承/**`extends`**|is-a|父子|
|实现|实现/**`implements`**|be-a/can-be-a|接口-实现|
|依赖|形参或局部变量|use-a|使用|
|关联|成员变量|has-a|平等关联|
|聚合|成员变量|contains-a|整体与部分<br/>生命周期不同|
|组合|成员变量|owns-a|整体与部分<br/>生命周期相同|

<!-- slide -->
# 泛化和实现的UML图

@import "./00_Diagrams/relation_of_class_level.puml"

<!-- slide -->
# 依赖的UML图

@import "./00_Diagrams/relation_of_class_dep.puml"

<!-- slide -->
# 关联、聚合和组合的UML图

@import "./00_Diagrams/relation_of_class_acc.puml"

<!-- slide -->
# 聚合的代码体现

```java{.line-numbers}
//Aggregation聚合
public class GooseGroup{
    private ArrayList<Goose> gooses = new ArrayList<Goose>();
    public GooseGroup(Goose gooses[]){//add gooses from outside
        for(Goose goose : gooses){
            this.gooses.add(goose);
        }
    }
    public Goose getGoose(int i){//can access goose
        return gooses.get(i);
    }
}
```

:point_right:“关联”和“聚合”在代码层面很难区别，一般区别在于语义上

<!-- slide -->
# 组合的代码体现

```java{.line-numbers}
public class Goose{
    private Wings wings;
    public Goose(){
        this.wings = new Wings();//create wings inside
    }
}
```

<!-- slide -->
# 4.7：实例成员与类成员:book:P77

## 用`static`修饰的成员（变量、方法或内部类）是类成员（静态成员），否则为实例成员

1. 实例成员属于 **具体的某个实例**，通过引用变量访问
1. 类成员属于类（所有实例对象共享），可通过引用变量或类名访问（一般通过类名访问）
1. 实例成员在创建实例里分配空间（方法分配存储入口地址的空间）
1. 类方法不可访问实例成员（实例对象此时还未创建）

<!-- slide -->
# 类方法（静态方法）与实例成员方法（实例方法）

## 怎样理解下面这句话:question:

:book:P80当我们创建第一个对象时，类中的实例方法就分配了入口地址，当再创建对象时，不再分配入口地址，也就是说，方法的入口地址被所有的对象共享，当所有的对象都不存在时，方法的入口地址才被取消

:point_right:方法在定义后即存在，当创建第一个对象后，类被动态链接加载到内存，即生成入口地址，当所有对象都不存在时，类可以从内存中卸载，即方法也从内存从卸载

<!-- slide -->
# 4.8：方法重载Overload与多态

1. **Overload**
    1. 作用： 使一个类中有多个相同名字的方法
    1. 要求： 形参必须不同，即 **形参个数不同** 或 **形参类型不同**（即“可分辨”）
1. **Overload** 是一种 **编译期多态** （即在编译完成后就已经确定调用哪种方法）

## :warning:不可以仅返回类型不同

<!-- slide -->
# 4.8.2：避免重载出现歧义

```java{.line-numbers}
class Dog{
    static  void cry(double m, int n){}
    static void cry(int m,double n){}
}
...
Dog.cry(10.0,10);//call void cry(double m, int n)
Dog.cry(10,10.0);//call void cry(int m,double n)
Dog.cry(10,10);//error, would be indistinguishable
```

<!-- slide -->
# 4.9： `this`关键字

## `this`即本对象的引用

1. 用于传递本对象本身
1. 用于构造方法中显式调用本对象的其他构造方法、成员变量或成员方法
1. 用于成员方法中显式调用本对象的成员变量或成员方法

:warning: **`static`** 方法中不能使用 **`this`**

<!-- slide -->
# `this`关键字

```java{.line-numbers}
public class Main {
    private int iData;
    private String sData;
    public Main(){
        this.iData = 100;//explicit use this variable
    }
    public Main(String sData){
        this();//use this constructor
        this.sData = sData;//explicit use this variable
    }
    public String getSData(){
        this.doSomething();//use this method
        return this.sData;//use this variable
    }
    private void doSomething(){
        new OtherClass(this).doOtherthing();//pass this
    }
    public static void staticDoSomething(){
        //cannot use "this" here
    }
}
```

<!-- slide -->
# 4.10：包

## 包`package`是用于解决名字冲突和组织管理类（和接口）的一个机制

1. **`package`** 必须为源文件的 **首条语句**，为该源文件中声明的类指定包名
1. **:warning::exclamation:包名和目录名必须一一对应**

```java{.line-numbers}
package <package_name>;//note: has semicolon here...

package tom.jiafei;
public class AClass{}
//<src_base>/tom/jiafei/AClass.java
//<out_base>/tom/jiafei/AClass.class
//pwd => <out_base>, and then => java tom.jiafei.AClass
```

<!-- slide -->
# 4.11：`import`语句

1. :point_right:**`import`** 后可直接通过类名或接口名访问，否则，应使用全路径（含包名）
1. :point_right:**`java.lang.*`** 已自动引入，无需再使用 **`import`**

<!-- slide -->
# 4.11.2：引入自定义包中的类

与系统包中的 **`import`** 语法一致，同样地，需在 **`classpath`** 下可寻找

<!-- slide -->
# 4.12：访问权限

1. 访问权限，即决定是否具有访问的权限（是否可写由 **`final`** 决定）
1. :warning:**“友好的访问权限”** 不太准确，**“默认访问权限”** 或 **“包访问权限”** 更准确
1. :warning:**`protected`** 和 **`private`** 不能修饰类和接口（内部类和内部接口除外）

<!-- slide -->
# 访问权限矩阵

1. **`public`：** 所有类均可访问
1. **`protected`：** 只有不同包的非子类不可访问
1. **default：** 只有同包的类（不分是不是子类）可访问
1. **`private`：** 只有本类可访问

|-|同一类|同一包|子类（不同包）|非子类（不同包）|
|-|:-:|:-:|:-:|:-:|:-:|
|**`public`**|:white_check_mark:|:white_check_mark:|:white_check_mark:|:white_check_mark:|
|**`protected`**|:white_check_mark:|:white_check_mark:|:white_check_mark:|:x:|
|**default**|:white_check_mark:|:white_check_mark:|:x:|:x:|
|**`private`**|:white_check_mark:|:x:|:x:|:x:|

<!-- slide -->
# 访问权限

```java{.line-numbers}
//SuperClass.java
package org.arws.demo.ch04.demoaccess;
public class SuperClass{
    public int pubData;
    protected int proData;
    int defData;
    private int priData;
    private static int static_priData;
    public void accessData(){
        //can access all
    }
}
```

<!-- slide -->
# 访问权限（续）

```java{.line-numbers}
//OtherClass.java
package org.arws.demo.ch04.demoaccess;
public class OtherClass{
    public void accessData(){
        //can access public , protected , default
        //cannot access private
    }
}
```

<!-- slide -->
# 访问权限（再续）

```java{.line-numbers}
//SubClass.java
package org.arws.demo.ch04.demoaccess.subpkg;
public class SubClass extends SuperClass{
    public void accessData(){
        //can access public , protected
        //cannot access default , private
    }
}
//OtherClass.java
package org.arws.demo.ch04.demoaccess.subpkg;
public classs OtherClass{
    public void accessData(){
        //can access public
        //cannot access protected, default, private
    }
}

```

<!-- slide -->
# 4.12.2：私有变量和私有方法

## :warning:`OOP`提倡“隐藏实现、开放接口”，从对象的变量和方法维度看，即通过开放方法以达到访问数据的目的

```java{.line-numbers}
public class demo{
    private int data = 100;
    public int getData(){
        <do_something>
        return this.data;
    }
    public void setData(int data){
        <do_something>
        this.data =  data;
    }
}
```

<!-- slide -->
# 4.13：基本类型的类封装

## `Boolean`, `Byte`, `Short`, `Integer`, `Long`, `Float`, `Double`, `Character`

1. 用于仅接收引用类型的情况，如集合类
1. 提供工具类方法，如 **`static String Integer.toString(int i, int radix)`**
1. 用于表征通用的异常情况（**`null`**）
1. 进一步封装基本数据类型的数据和操作，进一步完善以 **`Object`** 为单根的 **`OO`** 体系
1. 用于模拟值类型的指针传递，进行基本类型的多变量返回值

<!-- slide -->
# 4.14：对象数组

## 数组元素是对象的数组

<!-- slide -->
# 4.15：jar文件

1. **`jar`** 命令可将一组 **`class`** 打包压缩成一个 **`.jar`** 文件
1. 将 **`.jar`** 复制到 **`jre/lib/ext/`** 目录下（:warning:很少使用此种用法）

<!-- slide -->
# 4.16：`javadoc`文档生成器

<!-- slide -->
# 4.17：应用举例“搭建流水线”

```java{.line-numbers}
three = new ComputerAver();
two = new DelScore(three);
one = new InputScore(two);
```

<!-- slide -->
# 默认值

1. 基本类型的默认值为 **`0`**
1. 引用类型会默认为 **`null`**

<!-- slide -->
# 4.18：小结

<!-- slide -->
# 习题

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
