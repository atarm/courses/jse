---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第02章：基本数据类型和数组

<!-- slide -->
# 主要内容

1. 标识符与关键字
1. 基本数据类型（栈数据类型）
1. 数据类型转换
1. 数据输入输出
1. 数组

**:point_right: 回忆对比C语言，加深印象**

<!-- slide -->
# 2.1：标识符与关键字:book:P17

1. 标识符由字母、下划线、美元符号和数字组成，**长度不受限制**
1. 标识符的 **第一个字符不能是数字字符**
1. 标识符 **不能是关键字**（关键字都是小写）
1. 标识符 **不能是保留字**：**`true`**、**`false`** 和 **`null`**
1. 标识符 **区分大小写**

**:point_right: 非英文字母也可以作为标识符，但不建议**

**C语言标识符长度限制跟具体的编译器有关**

<!-- slide -->
# 2.2：基本数据类型:book:18

1. 逻辑类型（布尔类型）： **`boolean`**
1. 整数类型：**`byte`**(1B)、**`short`**(2B)、**`int`**(4B)、**`long`**(8B)
1. 浮点类型：**`float`**(4B)、**`double`**(8B)
1. 字符类型：**`char`**(2B，定长`Unicode`字符集)

## :warning: 没有无符号整型、`char`都是无符号、`float`字面量必须带后缀`f`或`F`

## :point_right: Java中除基本数据类型存储在栈内存中外，其他数据类型的值均不存储在栈内存中

<!-- slide -->
# 2.3：类型转换运算:book:P21

1. 隐式类型转换Implicit Casting：精度低的值赋值给精度高的变量
1. 显式类型转换Explicit Casting：精度高的值赋值给精度低的变量

<!-- slide -->
# 2.4：输入、输出数据:book:P23

```java{.line-numbers}
Scanner reader=new Scanner(System.in);
System.out.println();
System.out.printf("%d,%f",12, 23.78);
```

<!-- slide -->
# 2.5：数组:book:P24

## :warning: Java中的数组均是动态数组（堆对象）

## :warning: Java中的多维数组是数组的数组（每个元素是数组对象）

```java{.line-numbers}
public final class Array extends Object

public static void main(String args[]);

int main(int argc, char* argv[]);
```

<!-- slide -->
# 2.5.1：声明数组

```java{.line-numbers}
<element_type> <arrayname> [][];
<element_type> [][] <arrayname>;
<element_type>[] <arrayname>[];

int [] a, b[];
int a[],b[][];
```

<!-- slide -->
# 2.5.2：为数组分配元素空间

![f_2.4](./00_Images/f_2.4.png)

<!-- slide -->
# 2.5.3：数组元素的使用

1. 通过下标访问元素
1. 越界访问元素时将抛出`ArrayIndexOutOfBoundsException`

<!-- slide -->
# 2.5.4：`length`的使用

Java数组对象有`length`属性

<!-- slide -->
# 2.5.5：数组的初始化

<!-- slide -->
# 2.5.6：数组的引用

![f_2.5](./00_Images/f_2.5_sscdnf74o.png)

![f_2.6](./00_Images/f_2.6.png)

<!-- slide -->
# 2.6：应用举例

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok: