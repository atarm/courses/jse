---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第06章：接口与实现

<!-- slide -->
# 本章主要内容

1. **`interface`** 与 **`implements`**
1. 接口与多态
1. 接口变量作形参

<!-- slide -->
# 6.1：接口概述:book:P145

1. Java不支持多重继承，Java使用 **`interface`** 使一个类可以实现多个接口
1. 使用关键字interface来定义一个接口
1. 接口本质上也是 **`class`**，也产生 **`class文件`**，接口的访问权限修饰符与类一样，可以是 **`public`** 或 **default**

```java{.line-numbers}
interface <interface_name>{
    <interface_body>
}
```

<!-- slide -->
# 接口的变量和方法的规则

1. 接口中的成员变量均为 **`public static final`**
1. 接口中的成员方法均为 **`public abstract`**
1. 接口中的方法不能被 **`static`** 和 **`final`** 修饰（要重写接口中所有的方法）
1. 接口中没有构造函数

<!-- slide -->
# 6.2：实现接口:book:P146

```java{.line-numbers}
class <cls_name> extends <supercls_name> implements <if_name01>, <if_name02>,...{
}
```

1. 实现接口的类要成为非抽象类，则必须重写该接口的所有方法
1. 接口可以通过继承产生新的接口

<!-- slide -->
# 接口的细节说明:book:P147

```java{.line-numbers}
public interface SuperInterface {
    public static final int MAX_I_VALUE = 100;
    public void demoInterface();
}
interface SubInterface extends SuperInterface{
}
class SuperClass{
    public static final double MAX_F_VALUE = 200.01;
}
class DemoIMPL extends SuperClass implements  SubInterface{
    public void demoInterface() {
        System.out.println(MAX_I_VALUE);
        System.out.println(MAX_F_VALUE);
    }
}
```

<!-- slide -->
# 6.3：接口的UML图:book:P148

@import "./00_Diagram/interface_uml_diagram.puml"

<!-- slide -->
# 6.4：接口回调:book:P149

1. 用接口定义的变量称为接口变量，接口变量是一种引用型变量，接口变量存储实现了该接口的类的对象的引用
1. 在Java语言中，接口回调是指：可以把实现某一接口的类创建的对象的引用赋值给该接口声明的接口变量中，那么该接口变量就可以调用被类重写的接口方法

<!-- slide -->
# Callback

>In computer programming, a callback, also known as a **"call-after" function**, is any executable code that is passed as an argument to other code that is expected to call back (execute) the argument **at a given time**.

在Java中，除了接口还有哪些语法可以实现回调:question:

<!-- slide -->
# Example6_2:book:P150

```java{.line-numbers}
public class Example6_2 {
    public static void main(String args[]) {
        Display display = new Display();

        display.setSM(new TV());
        display.displayMessage();

        display.setSM(new PC());
        display.displayMessage();
    }
}
```

<!-- slide -->
# interface Callback

:point_right:即：实现了接口的某个类的对象向上转型赋值给该接口的接口变量，从而使主调函数调用该接口定义的成员方法

<!-- slide -->
# 6.5：理解接口:book:P150

1. 接口抽象出重要的行为标准，该行为标准用抽象方法来表示
1. 将实现接口的类的对象的引用赋值给接口变量，该接口变量可以调用被该类实现的接口方法，即体现该类根据接口里的行为标准给出的具体行为
1. 接口的思想在于它可以要求某些类有相同名称的方法，但方法的具体内容（方法体的内容）可以不同，接口在要求一些类有相同名称的方法的同时，并不强迫这些类具有相同的父类

<!-- slide -->
# 6.6：接口与多态:book:152

由接口产生的多态是指不同的类在实现同一接口时可能具有不同的实现方式，那么接口变量在回调接口方法时就可以具有多种形态

:point_right::book:P153 Example6_4.java

<!-- slide -->
# 6.7：接口参数:book:P153

如果一个方法的参数是接口类型，我们就可以将任何实现该接口的类的实例的引用传递给该接口参数，那么接口参数就可以回调类实现的接口方法

:point_right::book:P154 Example6_5.java

<!-- slide -->
# 6.8：abstract类与接口的比较:book:P154

1. **`abstract class`** 和 **`interface`** 都可以有 **`abstract method`**
1. **`interface`** 中只可以有常量，不可以有变量，**`abstract class`** 中即可以有常量也可以有变量
1. **`interface`** 中不能有 **`non-abstract method`**，**`abstract class`** 可以有 **`non-abstract method`**

<!-- slide -->
# 6.9：面向接口编程

1. 面向接口编程和面向对象编程并不是平级的
1. 面向接口编程并不是比面向对象编程更先进的一种独立的编程思想，而是附属于面向对象思想体系，属于其一部分，或者说，它是面向对象编程体系中的思想精髓之一

**面向接口编程** 是软件架构上的概念，范围大于具体编程语言的 **`interface`**

<!-- slide -->
# 接口的语义

1. 接口是一组规则（或能力）的集合，它规定了实现了本接口的类或接口必须拥有的一组规则（或能力），体现了自然界“如果你是……则必须能……”的理念，“如果你是大学生，就应该具备自学的能力”
1. 接口体现的是行为能力（**`-able`**）

<!-- slide -->
# 抽象类VS接口

1. 抽象层次不同
    + 抽象类是对实体抽象，接口是对行为抽象
    + 抽象类是对 **整个实体** 进行抽象，接口是对实体的 **局部行为** 进行抽象
1. 跨域不同
    + 抽象类跨域具有相似特点的类，而接口可以跨域不同的类
    + 抽象类体现继承关系，考虑的是子类与父类本质“是不是”同一类型的关系
    + 接口并不要求实现的类与接口是同一类型，它们之间只存在“有没有这个能力”的关系
1. 设计层次不同
    + 抽象类是自下而上的设计，在子类中重复出现的工作，抽象到抽象类中
    + 接口是自上而下，定义行为和规范

:point_right:抽象类定义了“是什么”，可以有非抽象的属性和方法；接口是更纯的抽象类，在 Java 中可以实现多个接口，因此接口表示“具有什么能力”

<!-- slide -->
# 使用抽象类还是接口

1. 是否表征一种实体:question:
1. 是否关注行为能力:question:

使用事物抽象时使用抽象类，在需要使用行为抽象时使用接口

<!-- slide -->
# 抽象类实现接口

```java{.line-numbers}
package java.awt.event;

public abstract Class KeyAdapter implements KeyListener
//An abstract adapter class for receiving keyboard events.
//The methods in this class are empty.
//This class exists as convenience for creating listener objects.
```

## :point_right:**`KeyAdapter`** 实现了 **`KeyListener`** 所有方法的空操作，从而使 **`KeyAdapter`** 的子类仅需Overriding其关注的事件方法，不关注的事件方法保持空操作

<!-- slide -->
# 抽象类实现接口（续）

```java{.line-numbers}
abstract class UDLRKeyListener extends KeyAdapter
void keyPressed(e){
    switch(e.code){
        case KeyEvent.VK_UP: upPressed(); break;
        case KeyEvent.VK_DOWN: downPressed(); break;
        case KeyEvent.VK_LEFT: leftPressed();break;
        case KeyEvent.VK_RIGHT: rightPressed(); break;
    }
}
void upPressed(){}
void downPressed(){}
void leftPressed(){}
void rightPressed(){}
```

<!-- slide -->
# 6.10：应用举例

要求：设计一个广告牌，希望所设计的广告牌可以展示许多公司的广告词

1. 问题的分析
1. 设计接口
1. 设计广告牌类

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
