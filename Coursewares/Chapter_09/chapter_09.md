---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# 第09章：组件及事件处理

<!-- slide -->
# 本章主要内容

1. Java GUI: AWT && SWING
1. GUI views: component, container, layout
    1. top container: frame && dialog
    1. middle container: pane
1. GUI events: event source, event object, event listener
1. MVC architecture
1. executable jar && GUI application

<!-- slide -->
# 本章重点难点

1. 重点：Swing包中的各种组件，各种布局和事件处理器的应用
1. 难点：各种事件处理器的使用

<!-- slide -->
# Java GUI框架

1. 官方GUI框架
    1. AWT(Abstract Window Toolkit)：1996Q1 SUN公司发布
    1. Swing：1998Q4 SUN公司发布
    1. JavaFX：2008Q4 SUN公司发布
1. 第三方GUI框架
    1. SWT(Standard Widget Toolkit)：IBM Eclipse于April 2003发布，通过JNI使用平台的本机小部件，与AWT/Swing无关

<!-- slide -->
# 9.1：Java Swing概述:book:221

<!-- slide -->
# Main Concepts

1. **component：** `Component`的子类或间接子类
1. **container：** `Container`的子类或间接子类，container可以容纳其他component
1. **layout：** container中的各component的排布规则
1. **event：** 发生在component的事件
1. **listener：** 注册监听事件然后进行callback处理

<!-- slide -->
# AWT and Swing

@import "./00_Diagram/component_tree.puml"

<!-- slide -->
# 9.2：JFrame:book:P222

1. **`JFrame`：** Swing的底层容器之一（即通常所称的窗口），其他组件必须被添加到底层容器中，以便借助这个底层容器和OS进行信息交互
1. 不能被添加到其他container： 窗口默认地被系统添加到显示器屏幕上，因此不允许将一个窗口添加到另一个容器中

<!-- slide -->
# 9.2.1：JFrame常用方法

```java{.line-numbers}
//constructor
JFrame();
JFrame(String s);
//release resource
public void dispose();
//show && validate && refresh
public void setVisible(boolean b);
public void validate();
//method
public void setContentPane(Container contentPane);
public void setLayout(LayoutManager mgr);
public Component add(Component comp);
public void setMenuBar(MenuBar mb);
//method
public void setBounds(int a,int b,int width,int height);
public void setSize(int width,int height);
public void setLocation(int x,int y);
public void setResizable(boolean b);
public void setExtendedState(int state);
public void setDefaultCloseOperation(int operation);
```

<!-- slide -->
# Example0901:book:P223

<!-- slide -->
# 9.2.2：菜单条、菜单、菜单项

1. `JMenuBar`：菜单栏
1. `JMenu`：菜单，菜单项container
1. `JMenuItem`：菜单项

<!-- slide -->
# Menus Inheritance

@import "./00_Diagram/component_menu.puml"

<!-- slide -->
# Example0902:book:P224

<!-- slide -->
# 9.3：常用组件与布局:book:P225

<!-- slide -->
# 9.3.1：常用组件

## 常用组件是`javax.swing.JComponent`的子类

```java{.line-numbers}
javax.swing.JLabel;
javax.swing.JButton;
javax.swing.JTextField;
javax.swing.JPasswordField;
javax.swing.JTextArea;
javax.swing.JCheckBox;
javax.swing.JRadioButton;
javax.swing.JComboBox;
```

<!-- slide -->
# Example0903:book:P226

<!-- slide -->
# 9.3.2：常用容器

1. `JComponent`是`Container`的子类，因此`JComponent`子类创建的组件也都是container
    1. 一般不将`JButton`、`JTextField`、`JTextArea`、`JCheckBox`等上节提到的组件当作container使用
    1. `JFrame`是底层容器，本节提到的容器被习惯地称做中间容器
1. 中间容器必须被添加到底层容器中才能发挥作用
1. panel=>面板，pane=>窗格，方框

<!-- slide -->
# 常见容器

@import "./00_Diagram/mid_container.puml"

<!-- slide -->
# JPanel

1. 向`JPanel`添加component，然后将这个`JPanel`添加到其它container中
1. `JPanel`的默认layout是`FlowLayout`布局

<!-- slide -->
# JTabbedPane

<!-- slide -->
# JScrollPane

1. `JScrollPane`只能添加一个component
1. 将component添加至`JScrollPane`后可使用滚动条

<!-- slide -->
# JSplitPane

1. 将窗格拆分成两部分的container
1. 拆分窗格有两种类型：水平拆分和垂直拆分

```java{.line-numbers}
//constructor
JSplitPane(int a,Component b,Component c);
JSplitPane(int a, boolean b,Component c,Component d);
public static final int HORIZONTAL_SPLIT = 1;
public static final int VERTICAL_SPLIT = 0;
```

<!-- slide -->
# JLayeredPane

![JLayeredPane_layers](./00_Image/JLayeredPane_layers.gif)

<!-- slide -->
# JLayeredPane Constants

```java{.line-numbers}
public static final Integer DEFAULT_LAYER = 0;
public static final Integer PALETTE_LAYER = 100;
public static final Integer MODAL_LAYER = 200;
public static final Integer POPUP_LAYER = 300;
public static final Integer DRAG_LAYER = 400;
public static final Integer FRAME_CONTENT_LAYER = -30000;
```

<!-- slide -->
# JFrame的Pane结构_01

![JFrame_layers](./00_Image/JFrame_layers_01.gif)

![JFrame_layers_02](./00_Image/JFrame_layers_02.gif)

<!-- slide -->
# JFrame的Pane结构_02

![JFrame_layers_03](./00_Image/JFrame_layers_03.png)

<!-- slide -->
# JFrame的Pane结构_03

@import "./00_Diagram/JFrame_Panes.gv"

<!-- slide -->
# JFrame的Pane结构_04

>The JFrame class is slightly incompatible with Frame. Like all other JFC/Swing top-level containers, a JFrame contains a JRootPane as its only child. The content pane provided by the root pane should, as a rule, contain all the non-menu components displayed by the JFrame. This is different from the AWT Frame case. As a convenience, the add, remove, and setLayout methods of this class are overridden, so that they delegate calls to the corresponding methods of the ContentPane. For example, you can add a child component to a frame as follows:
>
> **`frame.add(child);`**
>
>And the child will be added to the contentPane. The content pane will always be non-null. Attempting to set it to null will cause the JFrame to throw an exception. The default content pane will have a BorderLayout manager set on it.

<!-- slide -->
# JFrame的Pane结构_05

>**The glass pane:** **Hidden, by default**. If you make the glass pane visible, then it's like a sheet of glass over all the other parts of the root pane. It's completely transparent unless you implement the glass pane's paintComponent method so that it does something, and it can intercept input events for the root pane.
>**The layered pane:** **Serves to position its contents**, which consist of the content pane and the optional menu bar. Can also hold other components in a specified Z order.
>**The content pane:** **The container of the root pane's visible components**, excluding the menu bar.
>**The optional menu bar:** **The home for the root pane's container's menus**. If the container has a menu bar, you generally use the container's setJMenuBar method to put the menu bar in the appropriate place.

<!-- slide -->
# Further Reads

1. Using Top-Level Containers, https://docs.oracle.com/javase/tutorial/uiswing/components/toplevel.html
1. How to Use Root Panes, https://docs.oracle.com/javase/tutorial/uiswing/components/rootpane.html
1. How to Use Layered Panes, https://docs.oracle.com/javase/tutorial/uiswing/components/layeredpane.html

<!-- slide -->
# 9.3.3：常用布局

@import "./00_Diagram/layouts.puml"

<!-- slide -->
# set && get layout

```java{.line-numbers}
//java.awt.Container
public void setLayout(LayoutManager mgr);
public LayoutManager getLayout();
```

<!-- slide -->
# null Layout

1. layout为null的layout
1. component需要被准确定位到container的具体位置和大小

<!-- slide -->
# Example0904:book:P231 && Example0905:book:P232

<!-- slide -->
# 9. 4：处理事件:book:P233

event是异步处理中的重要概念

<!-- slide -->
# 9.4.1：事件处理模式

1. 事件源event source： 产生事件的对象
1. 事件监听器event listener： 监听（处理）事件的对象
1. 事件对象event object： Event Source传递给Event Listener的对象

<!-- slide -->
# Event Handle Architecture

@import "./00_Diagram/event_handle_arch.gv"

```java{.line-numbers}
class xxxEvent;
interface xxxListener;
addxxxListener();
removexxxListener();
```

<!-- slide -->
# Events

@import "./00_Diagram/events.puml"

<!-- slide -->
# 9.4.2：ActionEvent && Example0906:book:P235  && Example0907:book:P236

```java{.line-numbers}
//Event Source
TextField, Button, TextArea, List...;
//ActionListener
public void actionPerformed(ActionEvent e);
//ActionEvent
public Object getSource();
public String getActionCommand();
```

<!-- slide -->
# 9.4.3：ItemEvent && Example0908:book:P239

```java{.line-numbers}
//event source
List, ComboBox..;
//ItemListener
public void itemStateChanged(ItemEvent e);
//EventObject
public Object getSource();
//ItemEvent
public ItemSelectable getItemSelectable();
```

<!-- slide -->
# 9.4.4：DocumentEvent && Example0909:book:P242

```java{.line-numbers}
//event source
//javax.swing.text.JTextComponent
public Document getDocument();
//DocumentListener
public void changedUpdate(DocumentEvent e);
public void removeUpdate(DocumentEvent e);
public void insertUpdate(DocumentEvent e);
//DocumentEvent
DocumentEvent.ElementChange getChange(Element elem);
```

<!-- slide -->
# 9.4.5：MouseEvent && Example0910:book:P245 && Example0911:book:P247

```java{.line-numbers}
//event source
Component;
//MouseListener
public void mouseClicked(MouseEvent e);
public void mousePressed(MouseEvent e);
public void mouseReleased(MouseEvent e);
public void mouseEntered(MouseEvent e);
public void mouseExited(MouseEvent e);
//MouseMotionListener
public void mouseDragged(MouseEvent e);
public void mouseMoved(MouseEvent e);
//InputEvent
public int getModifiers();
//MouseEvent
public int getX();
public int getY();
public int getClickCount();
```

<!-- slide -->
# 9.4.6：FocusEvent && Example0912:book:P250

```java{.line-numbers}
//event source
Component;
//FocusListener
public void focusGained(FocusEvent e)
public void focusLost(FocusEvent e)
//java.awt.event.ComponentEvent
public Component getComponent()
```

<!-- slide -->
# 9.4.7：KeyEvent && Example0912:book:P250

```java{.line-numbers}
//event source
Component;
//KeyListener
public void keyPressed(KeyEvent e);
public void keyTyped(KeyEvent e);
public void KeyReleased(KeyEvent e);
//KeyEvent
public int getKeyCode();
public char getKeyChar();
```

<!-- slide -->
# 9.4.8：WindowListener && Example0913:book:P253

```java{.line-numbers}
//event source
JFrame,JDialog;
//WindowListener
public void windowActivated(WindowEvent e);
public void windowDeactivated(WindowEvent e);
public void windowClosing(WindowEvent e);
public void windowClosed(WindowEvent e);
public void windowIconified(WindowEvent e);
public void windowDeiconified(WindowEvent e);
public void windowOpened(WindowEvent e);
```

<!-- slide -->
# WindowAdapter && Example0913:book:P253

1. `Listener`对应的实现了所有方法（空操作）的`abstract Adapter`
    1. ComponentAdapter
    1. ContainerAdapter
    1. FocusAdapter
    1. KeyAdapter
    1. MouseAdapter
    1. MouseMotionAdapter
    1. WindowAdapter

<!-- slide -->
# 9.4.9：匿名类实例或窗口类作为Listener && Example0914:book:P254

1. 匿名类的方便之处是匿名类的外嵌类的成员变量在匿名类中仍然有效，当发生事件时，监视器就比较容易操作事件源所在的外嵌类中的成员.当事件的处理比较简单、系统也不复杂时，使用匿名类做监视器是一个不错的选择。
1. 让事件源所在的类的实例作为监视器，能让事件的处理比较方便，这是因为，监视器可以方便的操作事件源所在的类中的其他成员。当事件的处理比较简单，系统也不复杂时，让事件源所在的类的实例作为监视器是一个不错的选择。

<!-- slide -->
# 9.4.10：事件总结

1. **授权模式：** Java的事件处理基于授权模式，即事件源调用方法将某个对象注册为自己的监视器:question:注册模式
1. **接口回调：** `addXXXListener(XXXListener listener)`
1. **方法绑定：** 当事件源触发事件发生后，listener准确知道去调用哪个方法
1. **松耦合：** Interface-based programming, also known as interface-based architecture

<!-- slide -->
# 9. 5：MVC（Model-View-Controller）:book:P256

1. MVC是一种复合设计模式（或架构框架）
    1. 模型(model) ：业务逻辑，IPSO
    1. 视图(view)：显示逻辑
    1. 控制器(controller)： 控制逻辑
1. MVC的一般执行逻辑：
    1. 用户通过view或其他方式将input发送给controller
    1. controller选择并将input传递给model
    1. model处理后将output传递给controller
    1. controller选择并将output传递给view
    1. view将output显示给用户

<!-- slide -->
# [High Level Spring MVC](https://javarevisited.blogspot.com/2017/06/how-spring-mvc-framework-works-web-flow.html)

![high_level_SpringMVC](./00_Image/high_level_SpringMVC.png)

<!-- slide -->
# Example0915:book:P257

1. 为什么`JButton controlButton`不是controller:question: 哪个类的哪个方法才是controller:question:
1. 哪个类是model:question:哪个类是view:question:

<!-- slide -->
# [Spring MVC Flow](http://javawebtutor.com/articles/spring/spring-mvc-tutorial.php)

![spring-mvc-flow](./00_Image/spring-mvc-flow.png)

<!-- slide -->
# 9. 6：对话框:book:P259

1. `JDialog`和`JFrame`都是`Window`的子类，都是底层Container
1. `JDialog`须依赖于某个`JFrame`
1. dialog分为有模式和无模式
    1. 有模式Modal：堵塞其它线程的执行，不能再激活对话框所在程序中的其它窗口，直到该对话框消失不可见
    1. 无模式Modeless：能再激活其它窗口，也不堵塞其它线程的执行

<!-- slide -->
# 9.6.1：MessageDialog(Modal) && Example0916:book:P260

```java{.line-numbers}
//javax.swing.JOptionPane
public static void showMessageDialog(
    Component parentComponent, String message,
    String title, int messageType);
```

<!-- slide -->
# 9.6.2：InputDialog(Modal) && Example0917:book:P261

```java{.line-numbers}
//javax.swing.JOptionPane
public static String showInputDialog(
    Component parentComponent, Object message,
    String title, int messageType)
```

<!-- slide -->
# 9.6.3：ConfirmDialog(Modal) && Example0918:book:P263

```java{.line-numbers}
//javax.swing.JOptionPane
public static int showConfirmDialog(
    Component parentComponent, Object message,
    String title, int optionType);
```

<!-- slide -->
# 9.6.4：ColorChooserDialog(Modal or Modeless) && Example0919:book:P264

```java{.line-numbers}
//javax.swing.JColorChooser
public static JDialog createDialog(
    Component c, String title, boolean modal,
    JColorChooser chooserPane, ActionListener okListener,
    ActionListener cancelListener);

public static Color showDialog(//Modal
    Component component, String title, Color initialColor);
```

<!-- slide -->
# 9.6.5：自定义对话框 && Example0920:book:P265

```java{.line-numbers}
//JDialog default layout
BorderLayout;
/**
Creates a modeless dialog without a title and without a specified Frame owner.
A shared, hidden frame will be set as the owner of the dialog.
*/
public JDialog();
/**
Creates a modeless dialog with the specified Frame as its owner and an empty title.
If owner is null, a shared, hidden frame will be set as the owner of the dialog.
*/
public JDialog(Frame owner);
```

<!-- slide -->
# 9. 7：树组件与表格组件:book:P266

<!-- slide -->
# 9.7.1：`JTree`树组件

1. component：
    1. `javax.swing.JTree`
    1. `javax.swing.tree.DefaultMutableTreeNode`
1. listener: `javax.swing.event.TreeSelectionListener`
1. event: `javax.swing.event.TreeSelectionEvent`

<!-- slide -->
# `DefaultMutableTreeNode`

```java{.line-numbers}
/**Creates a tree node with no parent, no children, 
but which allows children, 
and initializes it with the specified user object.*/
DefaultMutableTreeNode(Object userObject)
/**Creates a tree node with no parent, no children,
initialized with the specified user object, 
and that allows children only if specified.
*/
DefaultMutableTreeNode(Object userObject, boolean allowsChildren)
```

<!-- slide -->
# `DefaultMutableTreeNode`的图像图标和文本描述

1. 图像图标： 根据结点类型（是否为叶子节点）自动显示不同的图像图标
1. 文本描述： `userObject.toString()`

<!-- slide -->
# Example0921:book:P268

<!-- slide -->
# 9.7.2：`JTable`表格组件

1. component:`javax.swing.JTable`
1. listener && event:
    1. `javax.swing.event.CellEditorListener`, `javax.swing.event.ChangeEvent`
    1. `javax.swing.event.ListSelectionListener`, `javax.swing.event.ListSelectionEvent`

<!-- slide -->
# `JTable`

```java{.line-numbers}
/**Constructs a default JTable that is initialized with a default data model,
a default column model, and a default selection model.*/
JTable()
/**Constructs a JTable with numRows and numColumns of empty cells
using DefaultTableModel.*/
JTable(int numRows, int numColumns)
/**Constructs a JTable to display the values in the two dimensional array,
rowData, with column names, columnNames.*/
JTable(Object[][] rowData, Object[] columnNames)

```

<!-- slide -->
# Example0922:book:P269

<!-- slide -->
# 9.8：按钮绑定到键盘:book:P271

```java{.line-numbers}
/**Parameters: condition - one of WHEN_IN_FOCUSED_WINDOW,
WHEN_FOCUSED, WHEN_ANCESTOR_OF_FOCUSED_COMPONENT*/
public final InputMap javax.swing.JComponent.getInputMap(int condition);

/**Adds a binding for keyStroke to actionMapKey.
If actionMapKey is null, this removes the current binding for keyStroke.*/
public void javax.swing.InputMap.put(KeyStroke keyStroke, Object actionMapKey);

public final ActionMap javax.swing.JComponent.getActionMap();

/**Adds a binding for key to action. If action is null,
this removes the current binding for key.*/
public void put(Object key, Action action)
```

<!-- slide -->
# 按钮绑定到键盘的步骤

1. 获取按钮的输入映射： `JComponent.getInputMap()`
1. 绑定按钮的键盘按键： `InputMap.put()`
1. 指定按钮的action映射的listener： `JComponent.getActionMap().put()`

<!-- slide -->
# Example0923:book:P272

<!-- slide -->
# 9.9：`PrintJob`打印组件:book:P273

```java{.line-numbers}
package java.awt;

public abstract class PrintJob extends Object;
public abstract class Toolkit extends Object;

//Returns: the toolkit of this component
public Toolkit java.awt.Component.getToolkit();
//Returns: the toolkit of this window
//Overrides: getToolkit in class Component
public Toolkit java.awt.Window.getToolkit();

public abstract PrintJob java.awt.Toolkit.getPrintJob(
    Frame frame, String jobtitle, Properties props)
public abstract Graphics java.awt.PrintJob.getGraphics()
//Overrides: printAll in class Component
public void javax.swing.JComponent.printAll(Graphics g)
//Overrides: print in class Container
public void javax.swing.JComponent.print(Graphics g)
```

<!-- slide -->
# Example0924:book:P274

<!-- slide -->
# 9. 10：发布GUI程序:book:P275

```bash{.line-numbers}
jar {ctxui}[vfmn0PMe] [jar-file] [manifest-file] [entry-point] [-C dir] files ...
-c  create new archive
-f  specify archive file name
-m  include manifest information from specified manifest file
```

<!-- slide -->
# `MANIFEST.MF`文件

1. 位于jar包 **`META-INF/MANIFEST.MF`**
1. 必选配置项：
    1. **`Manifest-Version`：** 本Manifest文件所遵循的格式版本
    1. **`Main-Class`：** 主类全路径
1. 重要配置项：
    1. **`Class-Path`：** 第三方依赖包的路径（一般为相对于jar包所在目录的相对路径）
1. 其他配置项：
    1. **`Created-By`：** 本Manifest文件的创建者，一般是构建工具的名称和版本号
    1. https://docs.oracle.com/javase/8/docs/technotes/guides/jar/index.html
    1. https://docs.oracle.com/javase/8/docs/technotes/guides/jar/jar.html

<!-- slide -->
# `MANIFEST.MF`文件注意事项

1. 文件末尾留空行： 否则无法识别最后一行配置
1. 配置项名称冒号后空一格，然后跟配置值
1. Class-Path的多个配置值之间用空格分隔
1. Class-Path一行过多配置值时（超过72个字符？）会出现 **`line too long`**，需分行书写（第二行行首应空两格）

<!--TODO
待明确是否是72个字符？还是72字节？
-->

<!-- slide -->
# 运行可执行jar包

1. jar包是一个zip压缩包
1. GUI： 鼠标双击、键盘enter（windows）
1. CLI： **`java -jar xxx.jar`**

<!-- slide -->
# 9.11：应用举例 && Example0925:book:P276

<!--
# JFrame JDialog JWindow JPanel

JFrame属于一个窗口，如显示器，有关闭按钮，有标题栏等
JPanel属于JFrame,一个JFrame能有多个JPanel,JPanel放的一些控件等；
Jpanel不是顶级窗口，不能直接输出。它必须放在象JFrame这样的顶级窗口上才能输出。
JcontentPane实际上就是一个JPanel。Jframe中会默认new一个JPanel，塞入JFrame中。
JPanel可以放在JFrame中，但是反过来就是不行的！效果上没什么特大的区别~！
JFrame用来做主页面框架，JPanel只是普通页面
JFrame  可以看成,最底级容器,可以包括其他上级容器包括JPanel
JFrame只是一个界面，也就是个框架，要想把控件放在该界面中，必须把控件放在JPanel中，然后再把JPanel放在JFrame中，JPanel作为一个容器使用。
JWindow是一个没有标题按钮的JFrame;

# AWT containers

1. Panel： 无边框container
1. Window: 有边框container
    1. Frame: 有放大缩小
    1. Dialog: 无放大缩小

# SWING containers

1. extends awt.Window: top containers
    1. JFrame
    1. JApplet
    1. JDialog
    1. JWindow
1. extends swing.JComponent: 无边框container

顶层容器：Jframe、Japplet、Jdialog、JWindow
中间容器：Jpanel、JscrollPane、JsplitPane、JToolBar
特殊容器：GUI 中特殊作用的中间层，例如 JinternalFrame、JlayeredPane、JRootPane
基本控件：人机交互的基本组件，如 Jbutton、JcomboBox、Jlist、Jmenu、Jslider、JtextField
信息显示组件：组件仅仅为显示信息，但不能编辑，如 Jlabel、JprogressBar、ToolTip
编辑信息组件：向用户显示可被编辑信息的组件，例如 JcolorChooser、JfileChoose、JfileChooser、Jtable、JtextArea

-->

<!-- slide -->
# Thank You

## :ok:End of This Chapter:ok:
