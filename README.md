# 面向对象程序设计（JavaSE）资料

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [目的](#目的)
2. [许可协议](#许可协议)
3. [结构](#结构)
4. [编写环境和使用方法](#编写环境和使用方法)
5. [对应教材](#对应教材)
    1. [教材基本信息](#教材基本信息)
    2. [教材配套资源](#教材配套资源)
6. [Java官方资源（英文）](#java官方资源英文)
7. [第三方网络资源（英文）](#第三方网络资源英文)
8. [第三方网络资源（中文）](#第三方网络资源中文)

<!-- /code_chunk_output -->

## 目的

记录、积累、分享并期望得到指正和补充。

如有任何错误或疏漏，烦请不吝指正。

## 许可协议

![CC_BY-SA_88x31](./00_Image/CC_BY-SA_88x31.png)

**本仓库使用`CC-BY-SA-4.0`协议，本仓库作者（署名）：aRoming**，简而言之：

**您可以自由地：**

1. **共享：** 在任何媒介以任何形式复制、发行本作品
1. **演绎：** 修改、转换或以本作品为基础进行创作在任何用途下，甚至商业目的

**您须遵守下列约定：**

1. **署名（BY）：** 您必须给出适当的署名，提供指向本许可协议的链接，同时标明是否（对原始作品）作了修改。您可以用任何合理的方式来署名，但是不得以任何方式暗示许可人为您或您的使用背书。
1. **相同方式共享（SA）：** 如果您再混合、转换或者基于本作品进行创作，您必须基于与原先许可协议相同的许可协议 分发您贡献的作品
1. **没有附加限制：** 您不得适用法律术语或者技术措施从而限制其他人做许可协议允许的事情。

详细的许可协议请见：

1. 本仓库[LICENSE](./LICENSE)
1. https://creativecommons.org/licenses/by-sa/4.0/
1. https://creativecommons.org/licenses/by-sa/4.0/deed.zh
1. https://choosealicense.com/licenses/cc-by-sa-4.0/#

## 结构

1. **`./Coursewares/*`：** 按教材章节在教材提供的课件基础上进行深层次重编的课件（使用 `markdown`编写的课件），认为教材及其课件不够准确以及缺少的部分进行修正和补充
1. **`./Exercises/*`：** 教材章节习题的答案和解析（原参考答案未提供解析），并修正了原参考答案存在的错误
1. **`./Extras/*`：** 另外收集补充的有关资料（目前仍很少）

:point_right:本仓库只包含对应教材的第01章至第10章

## 编写环境和使用方法

编写环境请参见[编写环境](./About/env.md)

使用方法请参见[使用方法](./About/usage.md)

## 对应教材

### 教材基本信息

1. 主教材：[《Java2实用教程（第5版）》（ISBN：9787302464259），清华大学出版社](http://www.tup.tsinghua.edu.cn/booksCenter/book_07342302.html)
1. 辅教材：[《Java2实用教程（第5版）实验指导与习题解答》（ISBN：9787302466871），清华大学出版社](http://www.tup.tsinghua.edu.cn/booksCenter/book_07356901.html)

### 教材配套资源

1. 清华大学出版社：
    1. Java 2实用教程（第5版）：http://www.tup.tsinghua.edu.cn/booksCenter/book_07342302.html
    1. Java 2实用教程（第5版）实验指导与习题解答：http://www.tup.tsinghua.edu.cn/booksCenter/book_07356901.html
1. 文泉课堂：https://www.wqketang.com/course/550/

## Java官方资源（英文）

1. The Java(TM) Tutorials: https://docs.oracle.com/javase/tutorial/index.html
1. Java Documentation: https://docs.oracle.com/en/java/index.html
1. Java Development Kit Version 13 Tool Specifications: https://docs.oracle.com/en/java/javase/13/docs/specs/man/index.html
1. Java Platform, Standard Edition & Java Development Kit
Version 13 API Specification: https://docs.oracle.com/en/java/javase/13/docs/api/index.html

## 第三方网络资源（英文）

1. W3Schools Java Tutorial: https://www.w3schools.in/java-tutorial/

## 第三方网络资源（中文）

1. Thinking in Java（未官方翻译版）：https://legacy.gitbook.com/book/quanke/think-in-java/details
1. 廖雪峰-Java教程： https://www.liaoxuefeng.com/wiki/1252599548343744
